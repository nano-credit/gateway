package sn.free.mfs.web.rest;

import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.web.multipart.MultipartFile;
import sn.free.mfs.domain.enumeration.AccessStatus;
import sn.free.mfs.repository.AccessListRepository;
import sn.free.mfs.service.AccessListService;
import sn.free.mfs.web.rest.errors.BadRequestAlertException;
import sn.free.mfs.service.dto.AccessListDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing {@link sn.free.mfs.domain.AccessList}.
 */
@RestController
@RequestMapping("/api")
public class AccessListResource {

    private final Logger log = LoggerFactory.getLogger(AccessListResource.class);

    private static final String ENTITY_NAME = "accessList";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AccessListService accessListService;

    private final AccessListRepository accessListRepository;

    public AccessListResource(AccessListService accessListService, AccessListRepository accessListRepository) {
        this.accessListService = accessListService;
        this.accessListRepository = accessListRepository;
    }

    /**
     * {@code POST  /access-lists} : Create a new accessList.
     *
     * @param accessListDTO the accessListDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new accessListDTO, or with status {@code 400 (Bad Request)} if the accessList has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/access-lists")
    public ResponseEntity<AccessListDTO> createAccessList(@RequestBody AccessListDTO accessListDTO) throws URISyntaxException {
        log.debug("REST request to save AccessList : {}", accessListDTO);
        Optional<AccessListDTO> existingOne = accessListService.findFirstByCin(accessListDTO.getCin());
        if(existingOne.isPresent()){
            AccessListDTO existingOneDto = existingOne.get();
            existingOneDto.setFirstName(accessListDTO.getFirstName());
            existingOneDto.setLastName(accessListDTO.getLastName());
            existingOneDto.setEmail(accessListDTO.getEmail());
            existingOneDto.setMsisdn(accessListDTO.getMsisdn());
            existingOneDto.setStatus(accessListDTO.getStatus());
            AccessListDTO result = accessListService.save(existingOneDto);
            return ResponseEntity.ok(result);
        }else{
            AccessListDTO result = accessListService.save(accessListDTO);
            return ResponseEntity.created(new URI("/api/access-lists/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                .body(result);
        }
    }


    /**
     * {@code PUT  /access-lists} : Updates an existing accessList.
     *
     * @param accessListDTO the accessListDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated accessListDTO,
     * or with status {@code 400 (Bad Request)} if the accessListDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the accessListDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/access-lists")
    public ResponseEntity<AccessListDTO> updateAccessList(@RequestBody AccessListDTO accessListDTO) throws URISyntaxException {
        log.debug("REST request to update AccessList : {}", accessListDTO);
        if (accessListDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AccessListDTO result = accessListService.save(accessListDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, accessListDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /access-lists} : get all the accessLists.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of accessLists in body.
     */
    @GetMapping("/access-lists")
    public ResponseEntity<List<AccessListDTO>> getAllAccessLists(Pageable pageable) {
        log.debug("REST request to get a page of AccessLists");
        Page<AccessListDTO> page = accessListService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /access-lists/:id} : get the "id" accessList.
     *
     * @param id the id of the accessListDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the accessListDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/access-lists/{id}")
    public ResponseEntity<AccessListDTO> getAccessList(@PathVariable Long id) {
        log.debug("REST request to get AccessList : {}", id);
        Optional<AccessListDTO> accessListDTO = accessListService.findOne(id);
        return ResponseUtil.wrapOrNotFound(accessListDTO);
    }

    /**
     * {@code DELETE  /access-lists/:id} : delete the "id" accessList.
     *
     * @param id the id of the accessListDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/access-lists/{id}")
    public ResponseEntity<Void> deleteAccessList(@PathVariable Long id) {
        log.debug("REST request to delete AccessList : {}", id);
        accessListService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/access-lists/blacklist/{msisdn}/{cni}")
    public ResponseEntity<AccessListDTO> blacklistUnpayer(@PathVariable String msisdn, @PathVariable String cni) throws URISyntaxException {
        log.debug("REST request to blackList msidn : {}, cni : {}", msisdn, cni);
        Optional<AccessListDTO> existingOne = accessListService.findFirstByCin(cni);
        if(existingOne.isPresent()){
            AccessListDTO existingOneDto = existingOne.get();
            existingOneDto.setStatus(AccessStatus.BLACKLISTED);
            AccessListDTO result = accessListService.save(existingOneDto);
            return ResponseEntity.ok(result);
        }
        else {
            AccessListDTO accessListDTO = new AccessListDTO();
            accessListDTO.setMsisdn(msisdn);
            accessListDTO.setCin(cni);
            accessListDTO.setFirstName("NA");
            accessListDTO.setLastName("NA");
            accessListDTO.setEmail("NA");
            accessListDTO.setStatus(AccessStatus.BLACKLISTED);
            AccessListDTO result = accessListService.save(accessListDTO);
            return ResponseEntity.ok(result);
        }
    }

    @GetMapping("/access-lists/search")
    public ResponseEntity<List<AccessListDTO>> getAccessListByMsisdnOrCinOrStatus(Pageable pageable, @RequestParam(required = false) String msisdn, @RequestParam(required = false) String cin, @RequestParam(required = false) String status) {
        AccessStatus statusEnum = null;
        try {

            statusEnum = AccessStatus.valueOf(status);
        }catch (IllegalArgumentException | NullPointerException  iae){
            log.debug(iae.getMessage());
        }
        log.debug("REST request to get Accesslist : {}", msisdn + " or " + cin);
        // List<AccessList> accessList = accessListService.findByMsisdnOrCin(msisdn, cin);
        List<AccessListDTO> all = accessListService.findByMsisdnOrCinOrStatus(pageable, msisdn, cin, statusEnum);
        return ResponseEntity.ok(all);
    }


    @GetMapping("/access-lists/bow/search")
    public ResponseEntity<AccessListDTO> getOneAccessList(@RequestParam String cin) {
        log.debug("REST request to get AL : {}", cin);
        Optional<AccessListDTO> acc = accessListService.findFirstByCin(cin);
        return ResponseUtil.wrapOrNotFound(acc);
    }

    @PostMapping("/access-lists/upload")
    public String bulkSimSwap(@RequestParam("file") MultipartFile file) throws IOException, URISyntaxException, JSONException {
        String txId = UUID.randomUUID().toString();
        this.log.debug(" bulking this file {} with transactionID {}", file);
        String result = this.accessListService.bulk(file.getInputStream());
        return result;

    }
}
