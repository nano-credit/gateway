package sn.free.mfs.web.rest.errors;

public class AccesListNotFoundException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public AccesListNotFoundException() {
        super(ErrorConstants.LOGIN_ALREADY_USED_TYPE, "AccessList not exist!", "accessListManagement", "accessnotexists");
    }
}
