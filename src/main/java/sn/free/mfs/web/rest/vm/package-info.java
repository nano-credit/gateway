/**
 * View Models used by Spring MVC REST controllers.
 */
package sn.free.mfs.web.rest.vm;
