package sn.free.mfs.web.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider;
import org.springframework.security.ldap.userdetails.LdapUserDetails;
import sn.free.mfs.domain.Authority;
import sn.free.mfs.domain.User;
import sn.free.mfs.security.MicrocredRoles;
import sn.free.mfs.security.jwt.JWTFilter;
import sn.free.mfs.security.jwt.TokenProvider;
import sn.free.mfs.service.UserService;
import sn.free.mfs.service.dto.UserDTO;
import sn.free.mfs.web.rest.vm.LoginVM;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class UserJWTController {

    private final TokenProvider tokenProvider;

    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final ActiveDirectoryLdapAuthenticationProvider adAuthProvider;
    private final UserDetailsService userDetailsService;
    private final UserService userService;


    public UserJWTController(TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder,
                             ActiveDirectoryLdapAuthenticationProvider adAuthProvider, UserDetailsService userDetailsService, UserService userService) throws Exception {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.adAuthProvider = adAuthProvider;
        this.userDetailsService = userDetailsService;

        adAuthProvider.setSearchFilter("(&(objectClass=user)(userPrincipalName={0}))");
        this.authenticationManagerBuilder.userDetailsService(userDetailsService);
        this.authenticationManagerBuilder.authenticationProvider(adAuthProvider);
        this.userService = userService;

    }

    @PostMapping("/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {

        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
        if (authentication == null) return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        Optional<User> userOptional = this.userService.getUserWithAuthoritiesByLogin(loginVM.getUsername());
        if (!userOptional.isPresent()) {
            LdapUserDetails userDetails = (LdapUserDetails) authentication.getPrincipal();
            final List<MicrocredRoles> roles = MicrocredRoles.allRoles.stream()
                .filter(r -> userDetails.getAuthorities().contains(r.getAuthority()))
                .collect(Collectors.toList());
            if (roles.isEmpty()) {
                log.error("{} filtered roles => {}", "JWT CONTROLLER", roles);
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            if (roles.contains(MicrocredRoles.ADMIN)) {
                roles.add(MicrocredRoles.MFS_ADMIN);
                roles.add(MicrocredRoles.MFS_AGENT);
                roles.add(MicrocredRoles.INTERN_SYSTEM);
            }
            roles.add(MicrocredRoles.USER);
            userOptional = Optional.of(this.userService.createUser(UserDTO.builder()
                .login(userDetails.getUsername())
                .firstName(userDetails.getDn()
                    .split(",")[0]
                    .replace("CN=", ""))
                .authorities(new HashSet<>(roles.stream()
                    .map(MicrocredRoles::getRole)
                    .collect(Collectors.toList())))
                .build()));
        }
        Set<String> roles = userOptional.get()
            .getAuthorities()
            .stream()
            .map(Authority::getName)
            .collect(Collectors.toSet());
        String jwt = tokenProvider.createToken(authentication, rememberMe, roles);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }
    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
}
