package sn.free.mfs.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.mfs.service.OfferService;
import sn.free.mfs.service.WalletService;
import sn.free.mfs.service.dto.OfferDTO;
import sn.free.mfs.service.dto.WalletDTO;
import sn.free.mfs.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.free.mfs.domain.Offer}.
 */
@RestController
@RequestMapping("/api")
public class OfferResource {

    private final Logger log = LoggerFactory.getLogger(OfferResource.class);

    private static final String ENTITY_NAME = "offer";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OfferService offerService;
    private final WalletService walletService;

    public OfferResource(OfferService offerService, WalletService walletService) {
        this.offerService = offerService;
        this.walletService = walletService;
    }

    /**
     * {@code POST  /offers} : Create a new offer.
     *
     * @param offerDTO the offerDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new offerDTO, or with status {@code 400 (Bad Request)} if the offer has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/offers")
    public ResponseEntity<OfferDTO> createOffer(@Valid @RequestBody OfferDTO offerDTO) throws Exception {
        log.debug("REST request to save Offer : {}", offerDTO);
        if (offerDTO.getId() != null) {
            throw new BadRequestAlertException("A new offer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OfferDTO result = offerService.save(offerDTO);
        return ResponseEntity.created(new URI("/api/offers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code POST  /offers/create} : Create a new offer from UI Form.
     *
     * @param offerVM the OfferVM to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new offerVM, or with status {@code 400 (Bad Request)} if the offer has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/offers/create")
    public ResponseEntity<OfferDTO> createNewOffer(@Valid @RequestBody OfferDTO offerVM) throws Exception {
        log.debug("REST request to save Offer from UI Form : {}", offerVM);
        if (offerVM.getId() != null) {
            throw new BadRequestAlertException("A new offer cannot already have an ID", ENTITY_NAME, "idexists");
        }

        WalletDTO loanWallet = walletService.save(offerVM.getLoanWallet());
        WalletDTO repaymentWallet = walletService.save(offerVM.getRepaymentWallet());
        WalletDTO feesWallet = walletService.save(offerVM.getFeesWallet());
        offerVM.setLoanWalletId(loanWallet.getId());
        offerVM.setRepaymentWalletId(repaymentWallet.getId());
        offerVM.setFeesWalletId(feesWallet.getId());

        OfferDTO result = offerService.save(offerVM);
        return ResponseEntity.created(new URI("/api/offers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /offers} : Updates an existing offer.
     *
     * @param offerDTO the offerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated offerDTO,
     * or with status {@code 400 (Bad Request)} if the offerDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the offerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/offers")
    public ResponseEntity<OfferDTO> updateOffer(@Valid @RequestBody OfferDTO offerDTO) throws Exception {
        log.debug("REST request to update Offer : {}", offerDTO);
        if (offerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        final OfferDTO o = offerService.findOne(offerDTO.getId()).orElseThrow(() -> new BadRequestAlertException("Invalid Loan ID", ENTITY_NAME, offerDTO.getId().toString()));
        offerDTO.getFeesWallet().setId(o.getFeesWalletId());
        offerDTO.getLoanWallet().setId(o.getLoanWalletId());
        offerDTO.getRepaymentWallet().setId(o.getRepaymentWalletId());

        final WalletDTO feesW = walletService.save(offerDTO.getFeesWallet());
        final WalletDTO loanW = walletService.save(offerDTO.getLoanWallet());
        final WalletDTO repaymentW = walletService.save(offerDTO.getRepaymentWallet());

        offerDTO.setLoanWalletId(loanW.getId());
        offerDTO.setRepaymentWalletId(repaymentW.getId());
        offerDTO.setFeesWalletId(feesW.getId());

        OfferDTO result = offerService.save(offerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, offerDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /offers} : get all the offers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of offers in body.
     */
    @GetMapping("/offers")
    public ResponseEntity<List<OfferDTO>> getAllOffers(Pageable pageable) {
        log.debug("REST request to get a page of Offers");
        Page<OfferDTO> page = offerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /offers/:id} : get the "id" offer.
     *
     * @param id the id of the offerDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the offerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/offers/{id}")
    public ResponseEntity<OfferDTO> getOffer(@PathVariable Long id) {
        log.debug("REST request to get Offer : {}", id);
        Optional<OfferDTO> offerDTO = offerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(offerDTO);
    }

    /**
     * {@code GET  /offers/slug/:slug} : get the offer with slug name "slug".
     *
     * @param slug the id of the offerDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the offerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/offers/slug/{slug}")
    public ResponseEntity<OfferDTO> getOffer(@PathVariable String slug) {
        log.debug("REST request to get Offer : {}", slug);
        Optional<OfferDTO> offerVM = offerService.findBySlug(slug);
        return ResponseUtil.wrapOrNotFound(offerVM);
    }

    /**
     * {@code DELETE  /offers/:id} : delete the "id" offer.
     *
     * @param id the id of the offerDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/offers/{id}")
    public ResponseEntity<Void> deleteOffer(@PathVariable Long id) {
        log.debug("REST request to delete Offer : {}", id);
        offerService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
