package sn.free.mfs.security;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @project: microcredit
 * @author: psow on 10/06/2020
 */
public enum MicrocredRoles {

    ADMIN("G_TERTIO_ADMIN", "ROLE_ADMIN"),
    MFS_ADMIN("G_MFS_ADMIN", "ROLE_MFS_ADMIN"),
    MFS_AGENT("G_MFS_AGENT", "ROLE_MFS_AGENT"),
    USER("G_MFS_USER", "ROLE_USER"),
    CALLCENTER("CALLCENTER", "ROLE_CC"),
    INTERN_SYSTEM("INTERNAL", "ROLE_INTERNAL");

    public static final List<MicrocredRoles> allRoles;

    static {
        List<MicrocredRoles> roles = new ArrayList<>(Arrays.asList(MicrocredRoles.values()));
        allRoles = Collections.unmodifiableList(roles);
    }

    @Getter
    private final String groupAD;
    @Getter
    private final String role;
    @Getter
    private final GrantedAuthority authority;

    MicrocredRoles(String groupAD, String role) {
        this.groupAD = groupAD;
        this.role = role;
        this.authority = new SimpleGrantedAuthority(groupAD);
    }

}
