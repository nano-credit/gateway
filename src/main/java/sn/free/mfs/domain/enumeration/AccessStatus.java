package sn.free.mfs.domain.enumeration;

/**
 * The AccessStatus enumeration.
 */
public enum AccessStatus {
    BLACKLISTED, WHITELISTED
}
