package sn.free.mfs.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Offer.
 */
@Entity
@Table(name = "offer")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Offer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "tenant_id", nullable = false, unique = true)
    private String tenantId;

    @NotNull
    @Column(name = "offer_name", nullable = false)
    private String offerName;

    @Column(name = "activated")
    private Boolean activated;

    @Column(name = "free_seniority")
    private Integer freeSeniority;

    @Column(name = "freemoney_seniority")
    private Integer freemoneySeniority;

    @Column(name = "default_loan_duration")
    private Integer defaultLoanDuration;

    @Column(name = "min_cust_age")
    private Integer minCustAge;

    @Column(name = "max_cust_age")
    private Integer maxCustAge;

    @Column(name = "max_simultaneous_loan")
    private Integer maxSimultaneousLoan;

    @Column(name = "max_loan_per_date")
    private Integer maxLoanPerDate;

    @Column(name = "max_current_balance")
    private Integer maxCurrentBalance;

    @Column(name = "min_amount")
    private Integer minAmount;

    @Column(name = "max_amount")
    private Integer maxAmount;

    @Column(name = "max_s1")
    private Integer maxS1;

    @Column(name = "max_s2")
    private Integer maxS2;

    @Column(name = "max_s3")
    private Integer maxS3;

    @Column(name = "max_s4")
    private Integer maxS4;

    @NotNull
    @Column(name = "total_fees_rate", nullable = false)
    private Integer totalFeesRate;

    @NotNull
    @Column(name = "min_repayment", nullable = false)
    private Integer minRepayment;

    @NotNull
    @Column(name = "pr_0_rate", nullable = false)
    private Integer pr0Rate;

    @NotNull
    @Column(name = "pr_1_rate", nullable = false)
    private Integer pr1Rate;

    @NotNull
    @Column(name = "pr_2_rate", nullable = false)
    private Integer pr2Rate;

    @Column(name = "msisdn_admin")
    private String msisdnAdmin;

    @NotNull
    @Column(name = "email_admin", nullable = false)
    private String emailAdmin;

    @OneToOne
    @JoinColumn(unique = true)
    private Wallet loanWallet;

    @OneToOne
    @JoinColumn(unique = true)
    private Wallet repaymentWallet;

    @OneToOne
    @JoinColumn(unique = true)
    private Wallet feesWallet;

    @NotNull
    @Column(name = "fees_freemoney_rate", nullable = false)
    private Integer feesFreeMoneyRate;

    @NotNull
    @Column(name = "fees_bank_rate", nullable = false)
    private Integer feesBankRate;

    @NotNull
    @Column(name = "nb_jour_pr1", nullable = false)
    private Integer nbJourPr1;

    @NotNull
    @Column(name = "nb_jour_pr2", nullable = false)
    private Integer nbJourPr2;

    @Column(name = "rappel_pr0")
    private String rappelPr0;

    @Column(name = "rappel_pr1")
    private String rappelPr1;

    @Column(name = "rappel_pr2")
    private String rappelPr2;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
