package sn.free.mfs.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Wallet.
 */
@Entity
@Table(name = "wallet")
public class Wallet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "msisdn", nullable = false)
    private String msisdn;

    @NotNull
    @Column(name = "pin", nullable = false)
    private String pin;

    @Column(name = "wallet_name")
    private String wallet_name;

    @Column(name = "wallet_type")
    private String wallet_type;

    @Column(name = "wallet_min_balance")
    private Float wallet_min_balance;

    @Column(name = "wallet_max_balance")
    private Float wallet_max_balance;

    @Column(name = "email_alerts_recipients_to")
    private String email_alerts_recipients_to;

    @Column(name = "email_alerts_recipients_cc")
    private String email_alerts_recipients_cc;

    @Column(name = "email_min_wallet_alert_template")
    private String email_min_wallet_alert_template;

    @Column(name = "email_max_wallet_alert_template")
    private String email_max_wallet_alert_template;

    @Column(name = "sms_alert_recipients")
    private String sms_alert_recipients;

    @Column(name = "sms_min_wallet_alert_template")
    private String sms_min_wallet_alert_template;

    @Column(name = "sms_max_wallet_alert_template")
    private String sms_max_wallet_alert_template;

    @Column(name = "spare1")
    private String spare1;

    @Column(name = "spare2")
    private String spare2;

    @Column(name = "spare3")
    private String spare3;

    @Column(name = "spare4")
    private String spare4;

    @Column(name = "spare5")
    private String spare5;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public Wallet msisdn(String msisdn) {
        this.msisdn = msisdn;
        return this;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPin() {
        return pin;
    }

    public Wallet pin(String pin) {
        this.pin = pin;
        return this;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getWallet_name() {
        return wallet_name;
    }

    public Wallet wallet_name(String wallet_name) {
        this.wallet_name = wallet_name;
        return this;
    }

    public void setWallet_name(String wallet_name) {
        this.wallet_name = wallet_name;
    }

    public String getWallet_type() {
        return wallet_type;
    }

    public Wallet wallet_type(String wallet_type) {
        this.wallet_type = wallet_type;
        return this;
    }

    public void setWallet_type(String wallet_type) {
        this.wallet_type = wallet_type;
    }

    public Float getWallet_min_balance() {
        return wallet_min_balance;
    }

    public Wallet wallet_min_balance(Float wallet_min_balance) {
        this.wallet_min_balance = wallet_min_balance;
        return this;
    }

    public void setWallet_min_balance(Float wallet_min_balance) {
        this.wallet_min_balance = wallet_min_balance;
    }

    public Float getWallet_max_balance() {
        return wallet_max_balance;
    }

    public Wallet wallet_max_balance(Float wallet_max_balance) {
        this.wallet_max_balance = wallet_max_balance;
        return this;
    }

    public void setWallet_max_balance(Float wallet_max_balance) {
        this.wallet_max_balance = wallet_max_balance;
    }

    public String getEmail_alerts_recipients_to() {
        return email_alerts_recipients_to;
    }

    public Wallet email_alerts_recipients_to(String email_alerts_recipients_to) {
        this.email_alerts_recipients_to = email_alerts_recipients_to;
        return this;
    }

    public void setEmail_alerts_recipients_to(String email_alerts_recipients_to) {
        this.email_alerts_recipients_to = email_alerts_recipients_to;
    }

    public String getEmail_alerts_recipients_cc() {
        return email_alerts_recipients_cc;
    }

    public Wallet email_alerts_recipients_cc(String email_alerts_recipients_cc) {
        this.email_alerts_recipients_cc = email_alerts_recipients_cc;
        return this;
    }

    public void setEmail_alerts_recipients_cc(String email_alerts_recipients_cc) {
        this.email_alerts_recipients_cc = email_alerts_recipients_cc;
    }

    public String getEmail_min_wallet_alert_template() {
        return email_min_wallet_alert_template;
    }

    public Wallet email_min_wallet_alert_template(String email_min_wallet_alert_template) {
        this.email_min_wallet_alert_template = email_min_wallet_alert_template;
        return this;
    }

    public void setEmail_min_wallet_alert_template(String email_min_wallet_alert_template) {
        this.email_min_wallet_alert_template = email_min_wallet_alert_template;
    }

    public String getEmail_max_wallet_alert_template() {
        return email_max_wallet_alert_template;
    }

    public Wallet email_max_wallet_alert_template(String email_max_wallet_alert_template) {
        this.email_max_wallet_alert_template = email_max_wallet_alert_template;
        return this;
    }

    public void setEmail_max_wallet_alert_template(String email_max_wallet_alert_template) {
        this.email_max_wallet_alert_template = email_max_wallet_alert_template;
    }

    public String getSms_alert_recipients() {
        return sms_alert_recipients;
    }

    public Wallet sms_alert_recipients(String sms_alert_recipients) {
        this.sms_alert_recipients = sms_alert_recipients;
        return this;
    }

    public void setSms_alert_recipients(String sms_alert_recipients) {
        this.sms_alert_recipients = sms_alert_recipients;
    }

    public String getSms_min_wallet_alert_template() {
        return sms_min_wallet_alert_template;
    }

    public Wallet sms_min_wallet_alert_template(String sms_min_wallet_alert_template) {
        this.sms_min_wallet_alert_template = sms_min_wallet_alert_template;
        return this;
    }

    public void setSms_min_wallet_alert_template(String sms_min_wallet_alert_template) {
        this.sms_min_wallet_alert_template = sms_min_wallet_alert_template;
    }

    public String getSms_max_wallet_alert_template() {
        return sms_max_wallet_alert_template;
    }

    public Wallet sms_max_wallet_alert_template(String sms_max_wallet_alert_template) {
        this.sms_max_wallet_alert_template = sms_max_wallet_alert_template;
        return this;
    }

    public void setSms_max_wallet_alert_template(String sms_max_wallet_alert_template) {
        this.sms_max_wallet_alert_template = sms_max_wallet_alert_template;
    }

    public String getSpare1() {
        return spare1;
    }

    public Wallet spare1(String spare1) {
        this.spare1 = spare1;
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public Wallet spare2(String spare2) {
        this.spare2 = spare2;
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public Wallet spare3(String spare3) {
        this.spare3 = spare3;
        return this;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public String getSpare4() {
        return spare4;
    }

    public Wallet spare4(String spare4) {
        this.spare4 = spare4;
        return this;
    }

    public void setSpare4(String spare4) {
        this.spare4 = spare4;
    }

    public String getSpare5() {
        return spare5;
    }

    public Wallet spare5(String spare5) {
        this.spare5 = spare5;
        return this;
    }

    public void setSpare5(String spare5) {
        this.spare5 = spare5;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Wallet)) {
            return false;
        }
        return id != null && id.equals(((Wallet) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Wallet{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", pin='" + getPin() + "'" +
            ", wallet_name='" + getWallet_name() + "'" +
            ", wallet_type='" + getWallet_type() + "'" +
            ", wallet_min_balance=" + getWallet_min_balance() +
            ", wallet_max_balance=" + getWallet_max_balance() +
            ", email_alerts_recipients_to='" + getEmail_alerts_recipients_to() + "'" +
            ", email_alerts_recipients_cc='" + getEmail_alerts_recipients_cc() + "'" +
            ", email_min_wallet_alert_template='" + getEmail_min_wallet_alert_template() + "'" +
            ", email_max_wallet_alert_template='" + getEmail_max_wallet_alert_template() + "'" +
            ", sms_alert_recipients='" + getSms_alert_recipients() + "'" +
            ", sms_min_wallet_alert_template='" + getSms_min_wallet_alert_template() + "'" +
            ", sms_max_wallet_alert_template='" + getSms_max_wallet_alert_template() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            ", spare4='" + getSpare4() + "'" +
            ", spare5='" + getSpare5() + "'" +
            "}";
    }
}
