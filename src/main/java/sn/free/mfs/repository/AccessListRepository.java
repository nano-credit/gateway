package sn.free.mfs.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import sn.free.mfs.domain.AccessList;
import sn.free.mfs.domain.Offer;
import sn.free.mfs.domain.enumeration.AccessStatus;
import sn.free.mfs.service.dto.AccessListDTO;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the AccessList entity.
 */
@Repository
public interface AccessListRepository extends JpaRepository<AccessList, Long> {
    //List<AccessList> findByMsisdnOrCin(String msisdn, String cin);
    Page<AccessList> findByMsisdnOrCinOrStatus(Pageable pageable, String msisdn, String cin, AccessStatus status);
    Optional<AccessList> findOneByMsisdn(String msisdn);

    List<AccessList> findByMsisdn(String msisdn);
    List<AccessList> findByCin(String cin);
    List<AccessList> findByStatus(AccessStatus status);

    Optional<AccessList> findOneByCin(String cin);

    AccessList findByCinAndMsisdnContains(String cin, String msisdn);


    Optional<AccessList> findFirstByCin(String cin);
}
