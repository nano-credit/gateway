package sn.free.mfs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import sn.free.mfs.service.PINCryptHelper;
import sn.free.mfs.service.WalletService;
import sn.free.mfs.domain.Wallet;
import sn.free.mfs.repository.WalletRepository;
import sn.free.mfs.service.dto.WalletDTO;
import sn.free.mfs.service.mapper.WalletMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Wallet}.
 */
@Service
@Transactional
public class WalletServiceImpl implements WalletService {

    private final Logger log = LoggerFactory.getLogger(WalletServiceImpl.class);

    private final WalletRepository walletRepository;
    private final WalletMapper walletMapper;
    private final PINCryptHelper pinCryptHelper;

    public WalletServiceImpl(WalletRepository walletRepository, WalletMapper walletMapper, PINCryptHelper pinCryptHelper) {
        this.walletRepository = walletRepository;
        this.walletMapper = walletMapper;
        this.pinCryptHelper = pinCryptHelper;
    }

    /**
     * Save a wallet.
     *
     * @param walletDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public WalletDTO save(WalletDTO walletDTO) throws Exception {
        log.debug("Request to save Wallet : {}", walletDTO);
        if (walletDTO.getPin().length() == 4) {
            walletDTO.setPin(pinCryptHelper.encryptText(walletDTO.getPin()));
        }
        Wallet wallet = walletMapper.toEntity(walletDTO);
        wallet = walletRepository.save(wallet);
        return walletMapper.toDto(wallet);
    }

    /**
     * Get all the wallets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WalletDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Wallets");
        return walletRepository.findAll(pageable)
            .map(walletMapper::toDto);
    }

    /**
     * Get one wallet by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WalletDTO> findOne(Long id) {
        log.debug("Request to get Wallet : {}", id);
        return walletRepository.findById(id)
            .map(walletMapper::toDto);
    }

    /**
     * Delete the wallet by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Wallet : {}", id);
        walletRepository.deleteById(id);
    }
}
