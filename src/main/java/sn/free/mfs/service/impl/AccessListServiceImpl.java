package sn.free.mfs.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import sn.free.mfs.domain.enumeration.AccessStatus;
import sn.free.mfs.service.AccessListService;
import sn.free.mfs.domain.AccessList;
import sn.free.mfs.repository.AccessListRepository;
import sn.free.mfs.service.dto.AccessListDTO;
import sn.free.mfs.service.dto.OfferDTO;
import sn.free.mfs.service.mapper.AccessListMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link AccessList}.
 */
@Service
@Transactional
public class AccessListServiceImpl implements AccessListService {

    private final Logger log = LoggerFactory.getLogger(AccessListServiceImpl.class);

    private final AccessListRepository accessListRepository;

    private final AccessListMapper accessListMapper;

    public AccessListServiceImpl(AccessListRepository accessListRepository, AccessListMapper accessListMapper) {
        this.accessListRepository = accessListRepository;
        this.accessListMapper = accessListMapper;
    }

    /**
     * Save a accessList.
     *
     * @param accessListDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AccessListDTO save(AccessListDTO accessListDTO) {
        log.debug("Request to save AccessList : {}", accessListDTO);
        AccessList accessList = accessListMapper.toEntity(accessListDTO);
        accessList = accessListRepository.save(accessList);
        return accessListMapper.toDto(accessList);
    }

    /**
     * Get all the accessLists.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AccessListDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AccessLists");
        return accessListRepository.findAll(pageable)
            .map(accessListMapper::toDto);
    }

    /**
     * Get one accessList by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AccessListDTO> findOne(Long id) {
        log.debug("Request to get AccessList : {}", id);
        return accessListRepository.findById(id)
            .map(accessListMapper::toDto);
    }

    /**
     * Delete the accessList by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AccessList : {}", id);
        accessListRepository.deleteById(id);
    }

    /**
     * Search all information for a specific access list.
     *
     * @param msisdn msisdn of access list to show.
     * @param cin    cin of access list to show.
     * @return the access list.
     */
    @Override
    public List<AccessListDTO> findByMsisdnOrCinOrStatus(Pageable pageable, String msisdn, String cin, AccessStatus status) {
        List<AccessList> accessLists = new ArrayList<>();
        if (StringUtils.isBlank(msisdn) && StringUtils.isBlank(cin) && status == null) {
            return this.findAll(Pageable.unpaged()).getContent();
        }
        if (StringUtils.isNotBlank(msisdn)) {
            accessLists.addAll(accessListRepository.findByMsisdn(msisdn));
        }
        if (StringUtils.isNotBlank(cin)) {
            accessLists.addAll(accessListRepository.findByCin(cin));
        }
        if (status != null) {
            accessLists.addAll(accessListRepository.findByStatus(status));
        }
        return accessLists.stream()
            .distinct()
            .map(accessListMapper::toDto)
            .collect(Collectors.toList());
    }


    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param accessListDTO user to update.
     * @return updated user.
     */

    @Override
    public Optional<AccessListDTO> update(AccessListDTO accessListDTO) {
        return Optional.of(accessListRepository
            .findOneByMsisdn(accessListDTO.getMsisdn()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(access -> {
                access.setStatus(accessListDTO.getStatus());
                log.debug("Changed Information for AccessList: {}", access);
                return access;
            })
            .map(accessListMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AccessListDTO> findFirstByCin(String cin) {
        log.debug("Request to get AccessList : {}", cin);
        return accessListRepository.findFirstByCin(cin)
            .map(accessListMapper::toDto);
    }



    public String bulk(InputStream inputStream) throws IOException, JSONException {
        // SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        JSONObject json = new JSONObject();
        Workbook workbook = new XSSFWorkbook(inputStream);
        AccessListDTO accessListDTO = new AccessListDTO();
        AccessList accessListFound = null;
        String msisdn = ""; String Cni2 = ""; String fname = ""; String lname = ""; String email = ""; String status = "";
        String msisdn2_row1 = "";
        double Cni1 = 0;
            XSSFSheet sheet = (XSSFSheet) workbook.getSheetAt(0);
            int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
            Row row1 = sheet.getRow(1);
            if (row1.getCell(0).toString() == "") {
                json.put("key", "Ligne 1: le MSISDN ne peut pas être vide");
                return json.toString();
            }
        try {
            double msisdn1_row1 = Double.parseDouble(row1.getCell(0).toString().replace(" ", ""));
            msisdn2_row1 = String.format("%.0f", msisdn1_row1);
        } catch (Exception e) {
            json.put("key", "Ligne " + 1 + ": format invalide du MSISDN. Format valide: 760000000 ou 221760000000");
            return json.toString();
        }
            if(msisdn2_row1.length() < 9){
                json.put("key", "Ligne 1: MSISDN incomplet");
                return json.toString();
            }


            for (int i = 1; i < rowCount + 1; i++) {

                Row row = sheet.getRow(i);
                System.out.printf("======================");
                System.out.printf(String.valueOf(rowCount));
                System.out.printf("======================");



                if (row.getCell(0).toString() == "" || row.getCell(1).toString() == "" || row.getCell(2).toString() == "" ||
                    row.getCell(3).toString() == "" || row.getCell(4).toString() == "" || row.getCell(5).toString() == "") {
                    json.put("key", "Ligne " + i + ": Tous les champs sont obligatoires.");
                    return json.toString();
                }

                try {
                    double msisdn2 = Double.parseDouble(row.getCell(0).toString().replace(" ", ""));
                    msisdn = String.format("%.0f", msisdn2);
                } catch (Exception e) {
                    json.put("key", "Ligne " + i + ": format invalide du MSISDN. Format valide: 760000000 ou 221760000000");
                    return json.toString();
                }


                if(msisdn.length() < 9 ){
                    json.put("key", "Ligne " + i + ": MSISDN incomplet");
                    return json.toString();
                }

                try{
                    Cni1 = Double.parseDouble(row.getCell(1).toString().replace(" ", ""));
                    Cni2 = String.format("%.0f", Cni1);
                } catch (Exception e) {
                    json.put("key", "Ligne " + i + ": Format CNI invalid.");
                    return json.toString();
                }

                status = row.getCell(5).toString();
                if(!status.equalsIgnoreCase("BLACKLISTED") && !status.equalsIgnoreCase("WHITELISTED")){
                    json.put("key", "Ligne " + i + ": status must have BLACKLISTED or WHITELISTED");
                    return json.toString();
                }
            }

            //save row 1
        Cni1 = Double.parseDouble(row1.getCell(1).toString().replace(" ", ""));
        Cni2 = String.format("%.0f", Cni1);
        fname = row1.getCell(2).toString();
        lname = row1.getCell(3).toString();
        email = row1.getCell(4).toString();
        status = row1.getCell(5).toString();
        accessListDTO.setMsisdn(msisdn2_row1); accessListDTO.setCin(Cni2); accessListDTO.setFirstName(fname);
        accessListDTO.setLastName(lname); accessListDTO.setEmail(email); accessListDTO.setStatus(AccessStatus.valueOf(status));
        // check if exist
         accessListFound = findByCinAndMsisdnContains(Cni2, msisdn2_row1);
        if(accessListFound != null){
            AccessListDTO accessListDTOFound = new AccessListDTO();
            accessListDTOFound.setId(accessListFound.getId()); accessListDTOFound.setCin(Cni2); accessListDTOFound.setMsisdn(msisdn2_row1);
            accessListDTOFound.setFirstName(fname); accessListDTOFound.setLastName(lname); accessListDTOFound.setEmail(email); accessListDTOFound.setStatus(AccessStatus.valueOf(status));
            this.save(accessListDTOFound);
        } else {
            this.save(accessListDTO);
        }

            // save from row2

            for (int i = 2; i < rowCount + 1; i++) {
                Row row = sheet.getRow(i);

                if (msisdn.length() > 12) {
                    msisdn = msisdn.substring(0, 12);
                }
                 Cni1 = Double.parseDouble(row.getCell(1).toString().replace(" ", ""));
                 Cni2 = String.format("%.0f", Cni1);
                fname = row.getCell(2).toString();
                lname = row.getCell(3).toString();
                email = row.getCell(4).toString();
                status = row.getCell(5).toString();

                accessListDTO.setMsisdn(msisdn); accessListDTO.setCin(Cni2); accessListDTO.setFirstName(fname);
                accessListDTO.setLastName(lname); accessListDTO.setEmail(email); accessListDTO.setStatus(AccessStatus.valueOf(status));
                accessListFound = findByCinAndMsisdnContains(Cni2, msisdn);
                if(accessListFound != null){
                    AccessListDTO accessListDTOFound = new AccessListDTO();
                    accessListDTOFound.setId(accessListFound.getId()); accessListDTOFound.setCin(Cni2); accessListDTOFound.setMsisdn(msisdn);
                    accessListDTOFound.setFirstName(fname); accessListDTOFound.setLastName(lname); accessListDTOFound.setEmail(email); accessListDTOFound.setStatus(AccessStatus.valueOf(status));
                    this.save(accessListDTOFound);
                } else {
                    this.save(accessListDTO);
                }

            }



        return null;

    }

    @Override
    public AccessList findByCinAndMsisdnContains(String cni, String msisdn) {
        return accessListRepository.findByCinAndMsisdnContains(cni, msisdn);
    }


}
