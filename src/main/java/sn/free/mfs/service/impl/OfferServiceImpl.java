package sn.free.mfs.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.mfs.domain.Offer;
import sn.free.mfs.repository.OfferRepository;
import sn.free.mfs.service.OfferService;
import sn.free.mfs.service.PINCryptHelper;
import sn.free.mfs.service.dto.OfferDTO;
import sn.free.mfs.service.mapper.OfferMapper;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Offer}.
 */
@Service
@Transactional
public class OfferServiceImpl implements OfferService {

    private final Logger log = LoggerFactory.getLogger(OfferServiceImpl.class);

    private final OfferRepository offerRepository;
    private final OfferMapper offerMapper;
    private final PINCryptHelper pinCryptHelper;

    public OfferServiceImpl(OfferRepository offerRepository, OfferMapper offerMapper, PINCryptHelper pinCryptHelper) {
        this.offerRepository = offerRepository;
        this.offerMapper = offerMapper;
        this.pinCryptHelper = pinCryptHelper;
    }

    /**
     * Save a offer.
     *
     * @param offerDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OfferDTO save(OfferDTO offerDTO) {
        log.debug("Request to save Offer : {}", offerDTO);
        Offer offer = offerMapper.toEntity(offerDTO);
        offer = offerRepository.save(offer);
        return offerMapper.toDto(offer);
    }

    /**
     * Get all the offers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OfferDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Offers");
        return offerRepository.findAll(pageable)
            .map(offerMapper::toDto);
    }

    /**
     * Get one offer by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OfferDTO> findOne(Long id) {
        log.debug("Request to get Offer : {}", id);
        return offerRepository.findById(id)
            .map(offerMapper::toDto);
    }

    /**
     * Get one offer by slug name.
     *
     * @param slug the slug name of the offer.
     * @return the offer.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OfferDTO> findBySlug(String slug) {
        log.debug("Request to get Offer : {}", slug);
        return offerRepository.findByTenantId(slug)
            .map(offerMapper::toDto);
    }

    /**
     * Delete the offer by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Offer : {}", id);
        offerRepository.deleteById(id);
    }
}
