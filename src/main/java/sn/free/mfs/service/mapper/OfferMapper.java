package sn.free.mfs.service.mapper;


import sn.free.mfs.domain.*;
import sn.free.mfs.service.dto.OfferDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Offer} and its DTO {@link OfferDTO}.
 */
@Mapper(componentModel = "spring", uses = {WalletMapper.class})
public interface OfferMapper extends EntityMapper<OfferDTO, Offer> {

    @Mapping(source = "loanWallet.id", target = "loanWalletId")
    @Mapping(source = "repaymentWallet.id", target = "repaymentWalletId")
    @Mapping(source = "feesWallet.id", target = "feesWalletId")
    OfferDTO toDto(Offer offer);

    @Mapping(source = "loanWalletId", target = "loanWallet")
    @Mapping(source = "repaymentWalletId", target = "repaymentWallet")
    @Mapping(source = "feesWalletId", target = "feesWallet")
    Offer toEntity(OfferDTO offerDTO);

    default Offer fromId(Long id) {
        if (id == null) {
            return null;
        }
        Offer offer = new Offer();
        offer.setId(id);
        return offer;
    }
}
