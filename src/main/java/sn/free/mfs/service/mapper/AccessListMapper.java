package sn.free.mfs.service.mapper;


import sn.free.mfs.domain.*;
import sn.free.mfs.service.dto.AccessListDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link AccessList} and its DTO {@link AccessListDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AccessListMapper extends EntityMapper<AccessListDTO, AccessList> {



    default AccessList fromId(Long id) {
        if (id == null) {
            return null;
        }
        AccessList accessList = new AccessList();
        accessList.setId(id);
        return accessList;
    }
}
