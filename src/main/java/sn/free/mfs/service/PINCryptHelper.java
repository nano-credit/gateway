package sn.free.mfs.service;

import com.google.common.io.ByteStreams;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * @project: microcredit
 * @author: psow on 11/08/2020
 */
@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class PINCryptHelper {

    private Cipher cipher;

    @Value("${app.security.private-key}")
    private String privateKeyFile;
    @Value("${app.security.public-key}")
    private String publicKeyFile;

    private final ResourceLoader resourceLoader;

    public PINCryptHelper(ResourceLoader resourceLoader) throws NoSuchPaddingException, NoSuchAlgorithmException {
        this.cipher = Cipher.getInstance("RSA");
        this.resourceLoader = resourceLoader;
    }

    private PrivateKey getPrivate() throws Exception {
        final Resource keyresource = resourceLoader.getResource("classpath:" + privateKeyFile);
        byte[] keyBytes = ByteStreams.toByteArray(keyresource.getInputStream());

        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }

    private PublicKey getPublic() throws Exception {
        final Resource publicKeyresource = resourceLoader.getResource("classpath:" + publicKeyFile);
        byte[] keyBytes = FileUtils.readFileToByteArray(publicKeyresource.getFile());
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }

    public String encryptText(String msg) throws Exception {
        this.cipher.init(Cipher.ENCRYPT_MODE, getPrivate());
        return Base64.encodeBase64String(cipher.doFinal(msg.getBytes(StandardCharsets.UTF_8)));
    }

    public String decryptText(String msg) throws Exception {
        this.cipher.init(Cipher.DECRYPT_MODE, getPublic());
        return new String(cipher.doFinal(Base64.decodeBase64(msg)), StandardCharsets.UTF_8);
    }

//    @PostConstruct
//    public void test() throws Exception {
//        final String s = encryptText("0000");
//        log.debug(s);
//        log.debug(decryptText(s));
//    }
}
