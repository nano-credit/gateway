package sn.free.mfs.service;

import org.springframework.boot.configurationprocessor.json.JSONException;
import sn.free.mfs.domain.AccessList;
import sn.free.mfs.domain.enumeration.AccessStatus;
import sn.free.mfs.service.dto.AccessListDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfs.domain.AccessList}.
 */
public interface AccessListService {

    /**
     * Save a accessList.
     *
     * @param accessListDTO the entity to save.
     * @return the persisted entity.
     */
    AccessListDTO save(AccessListDTO accessListDTO);

    /**
     * Get all the accessLists.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AccessListDTO> findAll(Pageable pageable);

    /**
     * Get the "id" accessList.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AccessListDTO> findOne(Long id);

    /**
     * Delete the "id" accessList.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    //List<AccessList> findByMsisdnOrCin(String msisdn, String cin);

    List<AccessListDTO> findByMsisdnOrCinOrStatus(Pageable pageable, String msisdn, String cin, AccessStatus status);

    Optional<AccessListDTO> update(AccessListDTO accessListDTO);

    Optional<AccessListDTO> findFirstByCin(String cin);
     String bulk(InputStream inputStream) throws IOException, JSONException;

     AccessList findByCinAndMsisdnContains(String cni, String msisdn);
}
