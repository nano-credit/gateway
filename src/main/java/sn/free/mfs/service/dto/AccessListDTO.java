package sn.free.mfs.service.dto;

import java.io.Serializable;
import java.util.Objects;
import sn.free.mfs.domain.enumeration.AccessStatus;

/**
 * A DTO for the {@link sn.free.mfs.domain.AccessList} entity.
 */
public class AccessListDTO implements Serializable {

    private Long id;

    private String msisdn;

    private String cin;

    private String firstName;

    private String lastName;

    private String email;

    private AccessStatus status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AccessStatus getStatus() {
        return status;
    }

    public void setStatus(AccessStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AccessListDTO accessListDTO = (AccessListDTO) o;
        if (accessListDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), accessListDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AccessListDTO{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", cin='" + getCin() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", email='" + getEmail() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
