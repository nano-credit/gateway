import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { IAccessList, AccessList } from 'app/shared/model/access-list.model';
import { AccessListService } from './access-list.service';

@Component({
  selector: 'jhi-access-list-update',
  templateUrl: './access-list-update.component.html'
})
export class AccessListUpdateComponent implements OnInit {
  isSaving = false;
  alert = '';
  isNotValidFormat = true;
  disable = true;
  loading = false;
  @ViewChild('form', { static: false }) form: any;
  file!: any;

  editForm = this.fb.group({
    id: [],
    msisdn: [[Validators.required]],
    cin: [[Validators.required]],
    firstName: [[Validators.required]],
    lastName: [[Validators.required]],
    email: [],
    status: [[Validators.required]]
  });

  constructor(
    protected accessListService: AccessListService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ accessList }) => {
      this.updateForm(accessList);
    });
  }

  updateForm(accessList: IAccessList): void {
    this.editForm.patchValue({
      id: accessList.id,
      msisdn: accessList.msisdn,
      cin: accessList.cin,
      firstName: accessList.firstName,
      lastName: accessList.lastName,
      email: accessList.email,
      status: accessList.status
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const accessList = this.createFromForm();
    if (accessList.id !== undefined) {
      this.subscribeToSaveResponse(this.accessListService.update(accessList));
    } else {
      this.subscribeToSaveResponse(this.accessListService.create(accessList));
    }
  }

  private createFromForm(): IAccessList {
    return {
      ...new AccessList(),
      id: this.editForm.get(['id'])!.value,
      msisdn: this.editForm.get(['msisdn'])!.value,
      cin: this.editForm.get(['cin'])!.value,
      firstName: this.editForm.get(['firstName'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      email: this.editForm.get(['email'])!.value,
      status: this.editForm.get(['status'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAccessList>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  uploadFile(): void {
    this.loading = true;
    const formData = new FormData();
    formData.append('file', this.file);
    this.accessListService.upload(formData).subscribe(res => {
      if (res !== null) {
        this.loading = false;
        this.alert = res.key;
        this.form.nativeElement.reset();
      } else {
        this.router.navigate(['access-list']);
      }
    });
  }
  onFileChange(event: any): void {
    this.disable = false;
    this.isNotValidFormat = true;
    this.file = event.target.files.length > 0 ? event.target.files[0] : null;

    if (!this.file.name.endsWith('.xlsx')) {
      this.isNotValidFormat = false;
      this.disable = true;
    }
  }
}
