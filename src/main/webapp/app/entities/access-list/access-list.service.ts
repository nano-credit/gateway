import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAccessList } from 'app/shared/model/access-list.model';

type EntityResponseType = HttpResponse<IAccessList>;
type EntityArrayResponseType = HttpResponse<IAccessList[]>;

@Injectable({ providedIn: 'root' })
export class AccessListService {
  public resourceUrl = SERVER_API_URL + 'api/access-lists';

  constructor(protected http: HttpClient) {}

  create(accessList: IAccessList): Observable<EntityResponseType> {
    return this.http.post<IAccessList>(this.resourceUrl, accessList, { observe: 'response' });
  }

  update(accessList: IAccessList): Observable<EntityResponseType> {
    return this.http.put<IAccessList>(this.resourceUrl, accessList, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAccessList>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAccessList[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAccessList[]>(`${this.resourceUrl}/search`, { params: options, observe: 'response' });
  }

  upload(formData: FormData): Observable<any> {
    return this.http.post<any>(`${this.resourceUrl}/upload`, formData);
  }
}
