import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAccessList } from 'app/shared/model/access-list.model';
import { AccessListService } from './access-list.service';

@Component({
  templateUrl: './access-list-delete-dialog.component.html'
})
export class AccessListDeleteDialogComponent {
  accessList?: IAccessList;

  constructor(
    protected accessListService: AccessListService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.accessListService.delete(id).subscribe(() => {
      this.eventManager.broadcast('accessListListModification');
      this.activeModal.close();
    });
  }
}
