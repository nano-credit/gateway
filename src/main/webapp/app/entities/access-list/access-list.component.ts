import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAccessList } from 'app/shared/model/access-list.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { AccessListService } from './access-list.service';
import { AccessListDeleteDialogComponent } from './access-list-delete-dialog.component';

import { AccessStatus } from 'app/shared/model/enumerations/access-status.model';

@Component({
  selector: 'jhi-access-list',
  templateUrl: './access-list.component.html'
})
export class AccessListComponent implements OnInit, OnDestroy {
  accessLists?: IAccessList[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  msisdn?: string;
  cin?: string;
  status?: AccessStatus;

  constructor(
    protected accessListService: AccessListService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.accessListService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IAccessList[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInAccessLists();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAccessList): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAccessLists(): void {
    this.eventSubscriber = this.eventManager.subscribe('accessListListModification', () => this.loadPage());
  }

  delete(accessList: IAccessList): void {
    const modalRef = this.modalService.open(AccessListDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.accessList = accessList;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'desc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IAccessList[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/access-list'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'desc' : 'desc')
      }
    });
    this.accessLists = data || [];
  }

  loadPageSearch(): void {
    this.accessListService
      .search({
        msisdn: this.msisdn || '',
        cin: this.cin || '',
        status: this.status || ''
      })
      .subscribe(
        (res: HttpResponse<IAccessList[]>) => this.onSuccess(res.body, res.headers, 1),
        () => this.onError()
      );
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  reset(): void {
    this.loadPage();
  }
}
