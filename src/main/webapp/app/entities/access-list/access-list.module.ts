import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { MicrocreditSharedModule } from 'app/shared/shared.module';
import { AccessListComponent } from './access-list.component';
import { AccessListDetailComponent } from './access-list-detail.component';
import { AccessListUpdateComponent } from './access-list-update.component';
import { AccessListDeleteDialogComponent } from './access-list-delete-dialog.component';
import { accessListRoute } from './access-list.route';

@NgModule({
  imports: [MicrocreditSharedModule, RouterModule.forChild(accessListRoute), FontAwesomeModule],
  declarations: [AccessListComponent, AccessListDetailComponent, AccessListUpdateComponent, AccessListDeleteDialogComponent],
  entryComponents: [AccessListDeleteDialogComponent]
})
export class MicrocreditAccessListModule {}
