import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAccessList, AccessList } from 'app/shared/model/access-list.model';
import { AccessListService } from './access-list.service';
import { AccessListComponent } from './access-list.component';
import { AccessListDetailComponent } from './access-list-detail.component';
import { AccessListUpdateComponent } from './access-list-update.component';

@Injectable({ providedIn: 'root' })
export class AccessListResolve implements Resolve<IAccessList> {
  constructor(private service: AccessListService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAccessList> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((accessList: HttpResponse<AccessList>) => {
          if (accessList.body) {
            return of(accessList.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AccessList());
  }
}

export const accessListRoute: Routes = [
  {
    path: '',
    component: AccessListComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'AccessLists'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AccessListDetailComponent,
    resolve: {
      accessList: AccessListResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'AccessLists'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AccessListUpdateComponent,
    resolve: {
      accessList: AccessListResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'AccessLists'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AccessListUpdateComponent,
    resolve: {
      accessList: AccessListResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'AccessLists'
    },
    canActivate: [UserRouteAccessService]
  }
];
