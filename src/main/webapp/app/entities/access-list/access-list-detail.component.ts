import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAccessList } from 'app/shared/model/access-list.model';

@Component({
  selector: 'jhi-access-list-detail',
  templateUrl: './access-list-detail.component.html'
})
export class AccessListDetailComponent implements OnInit {
  accessList: IAccessList | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ accessList }) => (this.accessList = accessList));
  }

  previousState(): void {
    window.history.back();
  }
}
