import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IOffer, Offer } from 'app/shared/model/offer.model';
import { OfferService } from './offer.service';
import { IWallet } from 'app/shared/model/wallet.model';
import { WalletService } from 'app/entities/wallet/wallet.service';

@Component({
  selector: 'jhi-offer-update',
  templateUrl: './offer-update.component.html'
})
export class OfferUpdateComponent implements OnInit {
  isSaving = false;
  loanwallets: IWallet[] = [];
  repaymentwallets: IWallet[] = [];
  feeswallets: IWallet[] = [];

  items = ['Javascript', 'Typescript'];

  editForm = this.fb.group({
    id: [],
    tenantId: [null, [Validators.required, Validators.minLength(3)]],
    offerName: [null, [Validators.required, Validators.minLength(3)]],
    activated: [],
    freeSeniority: [null, [Validators.max(999), Validators.min(1)]],
    freemoneySeniority: [null, [Validators.max(999), Validators.min(1)]],
    defaultLoanDuration: [null, [Validators.max(999), Validators.min(1)]],
    minCustAge: [null, [Validators.max(99), Validators.min(15)]],
    maxCustAge: [null, [Validators.max(99), Validators.min(15)]],
    maxSimultaneousLoan: [null, [Validators.max(9), Validators.min(1)]],
    maxLoanPerDate: [null, [Validators.max(9), Validators.min(1)]],
    maxCurrentBalance: [0, [Validators.max(999999), Validators.min(0)]],
    minAmount: [null, [Validators.max(999999), Validators.min(1000)]],
    maxAmount: [null, [Validators.max(999999), Validators.min(1000)]],
    totalFeesRate: [null, [Validators.required, Validators.max(100), Validators.min(0)]],
    minRepayment: [null, [Validators.required, Validators.min(100)]],
    pr0Rate: [null, [Validators.required, Validators.max(100), Validators.min(0)]],
    pr1Rate: [null, [Validators.required, Validators.max(100), Validators.min(0)]],
    pr2Rate: [null, [Validators.required, Validators.max(100), Validators.min(0)]],
    msisdnAdmin: [null, [Validators.minLength(9), Validators.maxLength(9)]],
    emailAdmin: [null, [Validators.required, Validators.email]],
    loanWallet: this.fb.group({
      msisdn: [null, [Validators.required, Validators.pattern('^\\d{2}\\d{3}\\d{4}$')]],
      pin: [null, [Validators.required, Validators.pattern('(^\\d{4}$)|(^.{172}$)')]]
    }),
    repaymentWallet: this.fb.group({
      msisdn: [null, [Validators.required, Validators.pattern('^\\d{2}\\d{3}\\d{4}$')]],
      pin: [null, [Validators.required, Validators.pattern('(^\\d{4}$)|(^.{172}$)')]]
    }),
    feesWallet: this.fb.group({
      msisdn: [null, [Validators.required, Validators.pattern('^\\d{2}\\d{3}\\d{4}$')]],
      pin: [null, [Validators.required, Validators.pattern('(^\\d{4}$)|(^.{172}$)')]]
    }),

    feesFreeMoneyRate: [null, [Validators.required, Validators.max(100), Validators.min(0)]],
    feesBankRate: [null, [Validators.required, Validators.max(100), Validators.min(0)]],
    nbJourPr1: [null, [Validators.required, Validators.max(100), Validators.min(0)]],
    nbJourPr2: [null, [Validators.required, Validators.max(100), Validators.min(0)]],
    rappelPr0: [null, [Validators.max(100), Validators.min(0)]],
    rappelPr1: [null, [Validators.max(100), Validators.min(0)]],
    rappelPr2: [null, [Validators.max(100), Validators.min(0)]],
    maxS1: [null, [Validators.required]],
    maxS2: [null, [Validators.required]],
    maxS3: [null, [Validators.required]],
    maxS4: [null, [Validators.required]]
  });

  constructor(
    protected offerService: OfferService,
    protected walletService: WalletService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ offer }) => {

      this.walletService
        .query({ filter: 'offer-is-null' })
        .pipe(
          map((res: HttpResponse<IWallet[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IWallet[]) => {
          if (!offer.loanWalletId) {
            this.loanwallets = resBody;
          } else {
            this.walletService
              .find(offer.loanWalletId)
              .pipe(
                map((subRes: HttpResponse<IWallet>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IWallet[]) => (this.loanwallets = concatRes));
          }
        });

      this.walletService
        .query({ filter: 'offer-is-null' })
        .pipe(
          map((res: HttpResponse<IWallet[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IWallet[]) => {
          if (!offer.repaymentWalletId) {
            this.repaymentwallets = resBody;
          } else {
            this.walletService
              .find(offer.repaymentWalletId)
              .pipe(
                map((subRes: HttpResponse<IWallet>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IWallet[]) => (this.repaymentwallets = concatRes));
          }
        });

      this.walletService
        .query({ filter: 'offer-is-null' })
        .pipe(
          map((res: HttpResponse<IWallet[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IWallet[]) => {
          if (!offer.feesWalletId) {
            this.feeswallets = resBody;
          } else {
            this.walletService
              .find(offer.feesWalletId)
              .pipe(
                map((subRes: HttpResponse<IWallet>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IWallet[]) => (this.feeswallets = concatRes));
          }
        });
      this.updateForm(offer);

    });
  }

  updateForm(offer: IOffer): void {

    const loanWalletMSISDN = offer.loanWallet ? offer.loanWallet.msisdn : '';
    const loanWalletPIN = offer.loanWallet ? offer.loanWallet.pin : '';

    const repaymentWalletMSISDN = offer.repaymentWallet ? offer.repaymentWallet.msisdn : '';
    const repaymentWalletPIN = offer.repaymentWallet ? offer.repaymentWallet.pin : '';

    const feesWalletMSISDN = offer.feesWallet ? offer.feesWallet.msisdn : '';
    const feesWalletPIN = offer.feesWallet ? offer.feesWallet.pin : '';

    this.editForm.patchValue({
      id: offer.id,
      tenantId: offer.tenantId,
      offerName: offer.offerName,
      activated: offer.activated,
      freeSeniority: offer.freeSeniority,
      freemoneySeniority: offer.freemoneySeniority,
      defaultLoanDuration: offer.defaultLoanDuration,
      minCustAge: offer.minCustAge,
      maxCustAge: offer.maxCustAge,
      maxSimultaneousLoan: offer.maxSimultaneousLoan,
      maxLoanPerDate: offer.maxLoanPerDate,
      maxCurrentBalance: offer.maxCurrentBalance,
      minAmount: offer.minAmount,
      maxAmount: offer.maxAmount,
      totalFeesRate: offer.totalFeesRate,
      minRepayment: offer.minRepayment,
      pr0Rate: offer.pr0Rate,
      pr1Rate: offer.pr1Rate,
      pr2Rate: offer.pr2Rate,
      msisdnAdmin: offer.msisdnAdmin,
      emailAdmin: offer.emailAdmin,

      loanWalletId: offer.loanWalletId,
      loanWallet: {
        msisdn: loanWalletMSISDN,
        pin: loanWalletPIN
      },
      // loanWalletPin: offer.loanWalletPin,

      repaymentWalletId: offer.repaymentWalletId,
      repaymentWallet: {
        msisdn: repaymentWalletMSISDN,
        pin: repaymentWalletPIN
      },
      // repaymentWalletMsisdn: offer.repaymentWalletMsisdn,
      // repaymentWalletPin: offer.repaymentWalletPin,

      feesWalletId: offer.feesWalletId,
      feesWallet: {
        msisdn: feesWalletMSISDN,
        pin: feesWalletPIN
      },
      // feesWalletMsisdn: offer.feesWalletMsisdn,
      // feesWalletPin: offer.feesWalletPin

      feesFreeMoneyRate: offer.feesFreeMoneyRate,
      feesBankRate: offer.feesBankRate,
      nbJourPr1: offer.nbJourPr1,
      nbJourPr2: offer.nbJourPr2,
      rappelPr0: offer.rappelPr0,
      rappelPr1: offer.rappelPr1,
      rappelPr2: offer.rappelPr2,
      maxS1: offer.maxS1,
      maxS2: offer.maxS2,
      maxS3: offer.maxS3,
      maxS4: offer.maxS4
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const offer = this.createFromForm();
    if (offer.id !== undefined) {
      this.subscribeToSaveResponse(this.offerService.update(offer));
    } else {
      this.subscribeToSaveResponse(this.offerService.create(offer));
    }
  }

  private createFromForm(): IOffer {
    return {
      ...new Offer(),
      id: this.editForm.get(['id'])!.value,
      tenantId: this.editForm.get(['tenantId'])!.value,
      offerName: this.editForm.get(['offerName'])!.value,
      activated: this.editForm.get(['activated'])!.value,
      freeSeniority: this.editForm.get(['freeSeniority'])!.value,
      freemoneySeniority: this.editForm.get(['freemoneySeniority'])!.value,
      defaultLoanDuration: this.editForm.get(['defaultLoanDuration'])!.value,
      minCustAge: this.editForm.get(['minCustAge'])!.value,
      maxCustAge: this.editForm.get(['maxCustAge'])!.value,
      maxSimultaneousLoan: this.editForm.get(['maxSimultaneousLoan'])!.value,
      maxLoanPerDate: this.editForm.get(['maxLoanPerDate'])!.value,
      maxCurrentBalance: this.editForm.get(['maxCurrentBalance'])!.value,
      minAmount: this.editForm.get(['minAmount'])!.value,
      maxAmount: this.editForm.get(['maxAmount'])!.value,
      totalFeesRate: this.editForm.get(['totalFeesRate'])!.value,
      minRepayment: this.editForm.get(['minRepayment'])!.value,
      pr0Rate: this.editForm.get(['pr0Rate'])!.value,
      pr1Rate: this.editForm.get(['pr1Rate'])!.value,
      pr2Rate: this.editForm.get(['pr2Rate'])!.value,
      msisdnAdmin: this.editForm.get(['msisdnAdmin'])!.value,
      emailAdmin: this.editForm.get(['emailAdmin'])!.value,
      loanWallet: this.editForm.get(['loanWallet'])!.value,
      repaymentWallet: this.editForm.get(['repaymentWallet'])!.value,
      feesWallet: this.editForm.get(['feesWallet'])!.value,

      feesFreeMoneyRate: this.editForm.get(['feesFreeMoneyRate'])!.value,
      feesBankRate: this.editForm.get(['feesBankRate'])!.value,
      nbJourPr1: this.editForm.get(['nbJourPr1'])!.value,
      nbJourPr2: this.editForm.get(['nbJourPr2'])!.value,
      rappelPr0: this.editForm.get(['rappelPr0'])!.value,
      rappelPr1: this.editForm.get(['rappelPr1'])!.value,
      rappelPr2: this.editForm.get(['rappelPr2'])!.value,
      maxS1: this.editForm.get(['maxS1'])!.value,
      maxS2: this.editForm.get(['maxS2'])!.value,
      maxS3: this.editForm.get(['maxS3'])!.value,
      maxS4: this.editForm.get(['maxS4'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOffer>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IWallet): any {
    return item.id;
  }

  private slugify(text: string): string {
    // const newText = text.split('').map(
    // (letter, i) => letter.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i)))

    return (
      text
        .toString() // Cast to string
        .toLowerCase() // Convert the string to lowercase letters
        .normalize('NFD')
        .trim() // Remove whitespace from both sides of a string
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(/&/g, '-y-') // Replace & with 'and'
        // eslint-disable-next-line
        .replace(/[^\w\-]+/g, '') // Remove all non-word chars
        // eslint-disable-next-line
        .replace(/\-\-+/g, '-')
    ); // Replace multiple - with single -
  }

  updateTenantId(): void {
    const nameField = this.editForm.get('offerName');
    if (!nameField) return;
    const slug = this.slugify(nameField.value);
    (this.editForm.get('tenantId') as AbstractControl).setValue(slug);
  }
}
