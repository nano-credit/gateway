import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IUMETransfer } from 'app/shared/model/loan/ume-transfer.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { UMETransferService } from './ume-transfer.service';
import { UMETransferDeleteDialogComponent } from './ume-transfer-delete-dialog.component';

@Component({
  selector: 'jhi-ume-transfer',
  templateUrl: './ume-transfer.component.html'
})
export class UMETransferComponent implements OnInit, OnDestroy {
  @Input() loanfileID = 0;

  uMETransfers?: IUMETransfer[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;

  constructor(
    protected uMETransferService: UMETransferService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.uMETransferService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IUMETransfer[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    if (this.loanfileID) {
      this.loadData(this.loanfileID);
    } else {
      this.activatedRoute.data.subscribe(data => {
        this.page = data.pagingParams.page;
        this.ascending = data.pagingParams.ascending;
        this.predicate = data.pagingParams.predicate;
        this.ngbPaginationPage = data.pagingParams.page;
        this.loadPage();
      });
    }
    this.registerChangeInUMETransfers();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IUMETransfer): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInUMETransfers(): void {
    this.eventSubscriber = this.eventManager.subscribe('uMETransferListModification', () => this.loadPage());
  }

  delete(uMETransfer: IUMETransfer): void {
    const modalRef = this.modalService.open(UMETransferDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.uMETransfer = uMETransfer;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IUMETransfer[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/ume-transfer'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.uMETransfers = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  private loadData(loanfileID?: number): void {
    this.uMETransferService.findByLoanId(loanfileID).subscribe(
      (res: HttpResponse<IUMETransfer[]>) => {
        this.uMETransfers = res.body as IUMETransfer[];
      },
      () => {
        console.error('Error fetching transfers');
      }
    );
    this.uMETransfers = [];
  }
}
