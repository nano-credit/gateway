import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IUMETransfer } from 'app/shared/model/loan/ume-transfer.model';

type EntityResponseType = HttpResponse<IUMETransfer>;
type EntityArrayResponseType = HttpResponse<IUMETransfer[]>;

@Injectable({ providedIn: 'root' })
export class UMETransferService {
  public resourceUrl = SERVER_API_URL + 'services/loan/api/ume-transfers';

  constructor(protected http: HttpClient) {}

  create(uMETransfer: IUMETransfer): Observable<EntityResponseType> {
    return this.http.post<IUMETransfer>(this.resourceUrl, uMETransfer, { observe: 'response' });
  }

  update(uMETransfer: IUMETransfer): Observable<EntityResponseType> {
    return this.http.put<IUMETransfer>(this.resourceUrl, uMETransfer, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUMETransfer>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUMETransfer[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findByLoanId(loanfileID: number | undefined) : Observable<EntityArrayResponseType> {
      return this.http.get<IUMETransfer[]>(`${this.resourceUrl}/loanfile/${loanfileID}`, { observe: 'response' });
  }
}
