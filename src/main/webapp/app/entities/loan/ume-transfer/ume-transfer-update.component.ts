import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IUMETransfer, UMETransfer } from 'app/shared/model/loan/ume-transfer.model';
import { UMETransferService } from './ume-transfer.service';
import { ILoanFile } from 'app/shared/model/loan/loan-file.model';
import { LoanFileService } from 'app/entities/loan/loan-file/loan-file.service';

@Component({
  selector: 'jhi-ume-transfer-update',
  templateUrl: './ume-transfer-update.component.html'
})
export class UMETransferUpdateComponent implements OnInit {
  isSaving = false;
  loanfiles: ILoanFile[] = [];

  editForm = this.fb.group({
    id: [],
    sourceWallet: [null, [Validators.required]],
    targetWallet: [null, [Validators.required]],
    amount: [null, [Validators.required]],
    type: [],
    paymentState: [],
    paymentTxId: [],
    paymentMessage: [],
    loanFileId: []
  });

  constructor(
    protected uMETransferService: UMETransferService,
    protected loanFileService: LoanFileService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ uMETransfer }) => {
      this.updateForm(uMETransfer);

      this.loanFileService.query().subscribe((res: HttpResponse<ILoanFile[]>) => (this.loanfiles = res.body || []));
    });
  }

  updateForm(uMETransfer: IUMETransfer): void {
    this.editForm.patchValue({
      id: uMETransfer.id,
      sourceWallet: uMETransfer.sourceWallet,
      targetWallet: uMETransfer.targetWallet,
      amount: uMETransfer.amount,
      type: uMETransfer.type,
      paymentState: uMETransfer.paymentState,
      paymentTxId: uMETransfer.paymentTxId,
      paymentMessage: uMETransfer.paymentMessage,
      loanFileId: uMETransfer.loanFileId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const uMETransfer = this.createFromForm();
    if (uMETransfer.id !== undefined) {
      this.subscribeToSaveResponse(this.uMETransferService.update(uMETransfer));
    } else {
      this.subscribeToSaveResponse(this.uMETransferService.create(uMETransfer));
    }
  }

  private createFromForm(): IUMETransfer {
    return {
      ...new UMETransfer(),
      id: this.editForm.get(['id'])!.value,
      sourceWallet: this.editForm.get(['sourceWallet'])!.value,
      targetWallet: this.editForm.get(['targetWallet'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      type: this.editForm.get(['type'])!.value,
      paymentState: this.editForm.get(['paymentState'])!.value,
      paymentTxId: this.editForm.get(['paymentTxId'])!.value,
      paymentMessage: this.editForm.get(['paymentMessage'])!.value,
      loanFileId: this.editForm.get(['loanFileId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUMETransfer>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ILoanFile): any {
    return item.id;
  }
}
