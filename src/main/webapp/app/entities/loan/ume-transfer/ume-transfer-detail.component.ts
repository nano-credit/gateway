import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUMETransfer } from 'app/shared/model/loan/ume-transfer.model';

@Component({
  selector: 'jhi-ume-transfer-detail',
  templateUrl: './ume-transfer-detail.component.html'
})
export class UMETransferDetailComponent implements OnInit {
  uMETransfer: IUMETransfer | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ uMETransfer }) => (this.uMETransfer = uMETransfer));
  }

  previousState(): void {
    window.history.back();
  }
}
