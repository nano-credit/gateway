import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUMETransfer } from 'app/shared/model/loan/ume-transfer.model';
import { UMETransferService } from './ume-transfer.service';

@Component({
  templateUrl: './ume-transfer-delete-dialog.component.html'
})
export class UMETransferDeleteDialogComponent {
  uMETransfer?: IUMETransfer;

  constructor(
    protected uMETransferService: UMETransferService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.uMETransferService.delete(id).subscribe(() => {
      this.eventManager.broadcast('uMETransferListModification');
      this.activeModal.close();
    });
  }
}
