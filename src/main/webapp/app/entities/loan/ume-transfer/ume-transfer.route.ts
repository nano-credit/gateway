import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IUMETransfer, UMETransfer } from 'app/shared/model/loan/ume-transfer.model';
import { UMETransferService } from './ume-transfer.service';
import { UMETransferComponent } from './ume-transfer.component';
import { UMETransferDetailComponent } from './ume-transfer-detail.component';
import { UMETransferUpdateComponent } from './ume-transfer-update.component';

@Injectable({ providedIn: 'root' })
export class UMETransferResolve implements Resolve<IUMETransfer> {
  constructor(private service: UMETransferService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUMETransfer> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((uMETransfer: HttpResponse<UMETransfer>) => {
          if (uMETransfer.body) {
            return of(uMETransfer.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new UMETransfer());
  }
}

export const uMETransferRoute: Routes = [
  {
    path: '',
    component: UMETransferComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'UMETransfers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: UMETransferDetailComponent,
    resolve: {
      uMETransfer: UMETransferResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'UMETransfers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: UMETransferUpdateComponent,
    resolve: {
      uMETransfer: UMETransferResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'UMETransfers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: UMETransferUpdateComponent,
    resolve: {
      uMETransfer: UMETransferResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'UMETransfers'
    },
    canActivate: [UserRouteAccessService]
  }
];
