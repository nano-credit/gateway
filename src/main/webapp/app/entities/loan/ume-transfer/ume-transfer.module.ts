import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MicrocreditSharedModule} from 'app/shared/shared.module';
import {UMETransferDetailComponent} from './ume-transfer-detail.component';
import {UMETransferUpdateComponent} from './ume-transfer-update.component';
import {UMETransferDeleteDialogComponent} from './ume-transfer-delete-dialog.component';
import {uMETransferRoute} from './ume-transfer.route';
import {LoanSharedModule} from "app/entities/loan/loan-shared.module";

@NgModule({
  imports: [MicrocreditSharedModule, RouterModule.forChild(uMETransferRoute), LoanSharedModule],
  declarations: [UMETransferDetailComponent, UMETransferUpdateComponent, UMETransferDeleteDialogComponent],
  entryComponents: [UMETransferDeleteDialogComponent],
})
export class LoanUMETransferModule {
}
