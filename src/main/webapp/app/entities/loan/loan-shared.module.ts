import {NgModule} from '@angular/core';
import {UMETransferComponent} from "app/entities/loan/ume-transfer/ume-transfer.component";
import {MicrocreditSharedModule} from "app/shared/shared.module";
import {RouterModule} from "@angular/router";

@NgModule({
  imports:[MicrocreditSharedModule, RouterModule ],
  declarations: [UMETransferComponent],
  exports: [UMETransferComponent]
})
export class LoanSharedModule {}
