import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ILoanFile } from 'app/shared/model/loan/loan-file.model';

type EntityResponseType = HttpResponse<ILoanFile>;
type EntityArrayResponseType = HttpResponse<ILoanFile[]>;

@Injectable({ providedIn: 'root' })
export class LoanFileService {
  public resourceUrl = SERVER_API_URL + 'services/loan/api/loan-files';

  constructor(protected http: HttpClient) {}

  create(loanFile: ILoanFile): Observable<EntityResponseType> {
    return this.http.post<ILoanFile>(this.resourceUrl, loanFile, { observe: 'response' });
  }

  update(loanFile: ILoanFile): Observable<EntityResponseType> {
    return this.http.put<ILoanFile>(this.resourceUrl, loanFile, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ILoanFile>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ILoanFile[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ILoanFile[]>(`${this.resourceUrl}/search`, { params: options, observe: 'response' });
  }
}
