import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ILoanFile, LoanFile } from 'app/shared/model/loan/loan-file.model';
import { LoanFileService } from './loan-file.service';

@Component({
  selector: 'jhi-loan-file-update',
  templateUrl: './loan-file-update.component.html'
})
export class LoanFileUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    msisdn: [null, [Validators.required]],
    amount: [null, [Validators.required]],
    offerSlug: [null, [Validators.required]],
    balance: [],
    idType: [],
    idNumber: [],
    eligibilityScore: [],
    grantingStatus: [],
    fees: [],
    grantedAmount: []
  });

  constructor(protected loanFileService: LoanFileService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ loanFile }) => {
      this.updateForm(loanFile);
    });
  }

  updateForm(loanFile: ILoanFile): void {
    this.editForm.patchValue({
      id: loanFile.id,
      msisdn: loanFile.msisdn,
      amount: loanFile.amount,
      offerSlug: loanFile.offerSlug,
      balance: loanFile.balance,
      idType: loanFile.idType,
      idNumber: loanFile.idNumber,
      eligibilityScore: loanFile.eligibilityScore,
      grantingStatus: loanFile.grantingStatus,
      fees: loanFile.fees,
      grantedAmount: loanFile.grantedAmount
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const loanFile = this.createFromForm();
    if (loanFile.id !== undefined) {
      this.subscribeToSaveResponse(this.loanFileService.update(loanFile));
    } else {
      this.subscribeToSaveResponse(this.loanFileService.create(loanFile));
    }
  }

  private createFromForm(): ILoanFile {
    return {
      ...new LoanFile(),
      id: this.editForm.get(['id'])!.value,
      msisdn: this.editForm.get(['msisdn'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      offerSlug: this.editForm.get(['offerSlug'])!.value,
      balance: this.editForm.get(['balance'])!.value,
      idType: this.editForm.get(['idType'])!.value,
      idNumber: this.editForm.get(['idNumber'])!.value,
      eligibilityScore: this.editForm.get(['eligibilityScore'])!.value,
      grantingStatus: this.editForm.get(['grantingStatus'])!.value,
      fees: this.editForm.get(['fees'])!.value,
      grantedAmount: this.editForm.get(['grantedAmount'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILoanFile>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
