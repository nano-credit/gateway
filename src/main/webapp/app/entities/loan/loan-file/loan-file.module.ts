import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MicrocreditSharedModule} from 'app/shared/shared.module';
import {LoanFileComponent} from './loan-file.component';
import {LoanFileDetailComponent} from './loan-file-detail.component';
import {LoanFileUpdateComponent} from './loan-file-update.component';
import {LoanFileDeleteDialogComponent} from './loan-file-delete-dialog.component';
import {loanFileRoute} from './loan-file.route';
import {LoanSharedModule} from "app/entities/loan/loan-shared.module";

@NgModule({
  imports: [MicrocreditSharedModule, RouterModule.forChild(loanFileRoute), LoanSharedModule],
  declarations: [LoanFileComponent, LoanFileDetailComponent, LoanFileUpdateComponent, LoanFileDeleteDialogComponent],
  entryComponents: [LoanFileDeleteDialogComponent]
})
export class LoanLoanFileModule {
}
