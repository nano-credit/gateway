import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ILoanFile, LoanFile } from 'app/shared/model/loan/loan-file.model';
import { LoanFileService } from './loan-file.service';
import { LoanFileComponent } from './loan-file.component';
import { LoanFileDetailComponent } from './loan-file-detail.component';
import { LoanFileUpdateComponent } from './loan-file-update.component';

@Injectable({ providedIn: 'root' })
export class LoanFileResolve implements Resolve<ILoanFile> {
  constructor(private service: LoanFileService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ILoanFile> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((loanFile: HttpResponse<LoanFile>) => {
          if (loanFile.body) {
            return of(loanFile.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new LoanFile());
  }
}

export const loanFileRoute: Routes = [
  {
    path: '',
    component: LoanFileComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,desc',
      pageTitle: 'LoanFiles'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: LoanFileDetailComponent,
    resolve: {
      loanFile: LoanFileResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'LoanFiles'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: LoanFileUpdateComponent,
    resolve: {
      loanFile: LoanFileResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'LoanFiles'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: LoanFileUpdateComponent,
    resolve: {
      loanFile: LoanFileResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'LoanFiles'
    },
    canActivate: [UserRouteAccessService]
  }
];
