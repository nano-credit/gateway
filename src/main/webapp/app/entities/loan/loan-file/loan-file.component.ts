import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ILoanFile } from 'app/shared/model/loan/loan-file.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { LoanFileService } from './loan-file.service';
import { LoanFileDeleteDialogComponent } from './loan-file-delete-dialog.component';
import { GrantingStatus } from 'app/shared/model/enumerations/granting-status.model';

@Component({
  selector: 'jhi-loan-file',
  templateUrl: './loan-file.component.html'
})
export class LoanFileComponent implements OnInit, OnDestroy {
  loanFiles?: ILoanFile[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  msisdn?: string;
  offer?: string;
  debut?: string;
  fin?: string;
  status?: GrantingStatus;

  constructor(
    protected loanFileService: LoanFileService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.loanFileService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<ILoanFile[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  initializeSearch(): void {
    this.msisdn = '';
    this.offer = undefined;
    this.status = undefined;
    this.debut = '';
    this.fin = '';
    this.loadPage(1);
  }

  loadPageSearch(page?: number): void {
    const pageToLoad: number = page || this.page;
    // eslint-disable-next-line no-console
    console.log('dates');
    // eslint-disable-next-line no-console
    console.log(this.debut);
    // eslint-disable-next-line no-console
    console.log(this.fin);
    this.loanFileService
      .search({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
        msisdn: this.msisdn || '',
        offer: this.offer || '',
        status: this.status || '',
        debut: this.debut !== undefined && this.debut !== '' ? new Date(this.debut).toISOString() : '',
        fin: this.fin !== undefined && this.fin !== '' ? new Date(this.fin).toISOString() : ''
      })
      .subscribe(
        (res: HttpResponse<ILoanFile[]>) => this.onSuccess(res.body, res.headers, 1),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInLoanFiles();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ILoanFile): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInLoanFiles(): void {
    this.eventSubscriber = this.eventManager.subscribe('loanFileListModification', () => this.loadPage());
  }

  delete(loanFile: ILoanFile): void {
    const modalRef = this.modalService.open(LoanFileDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.loanFile = loanFile;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: ILoanFile[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/loan-file'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    // eslint-disable-next-line no-console
    console.log('DATA RETRIEVED :' + data);
    this.loanFiles = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }
}
