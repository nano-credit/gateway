import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ILoanFile } from 'app/shared/model/loan/loan-file.model';
import { LoanFileService } from './loan-file.service';

@Component({
  templateUrl: './loan-file-delete-dialog.component.html'
})
export class LoanFileDeleteDialogComponent {
  loanFile?: ILoanFile;

  constructor(protected loanFileService: LoanFileService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.loanFileService.delete(id).subscribe(() => {
      this.eventManager.broadcast('loanFileListModification');
      this.activeModal.close();
    });
  }
}
