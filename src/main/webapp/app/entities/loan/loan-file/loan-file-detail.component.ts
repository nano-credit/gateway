import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ILoanFile } from 'app/shared/model/loan/loan-file.model';

@Component({
  selector: 'jhi-loan-file-detail',
  templateUrl: './loan-file-detail.component.html'
})
export class LoanFileDetailComponent implements OnInit {
  loanFile: ILoanFile | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ loanFile }) => (this.loanFile = loanFile));
  }

  previousState(): void {
    window.history.back();
  }
}
