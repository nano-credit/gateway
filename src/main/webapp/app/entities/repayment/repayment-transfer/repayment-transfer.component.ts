import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IRepaymentTransfer } from 'app/shared/model/repayment/repayment-transfer.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { RepaymentTransferService } from './repayment-transfer.service';
import { RepaymentTransferDeleteDialogComponent } from './repayment-transfer-delete-dialog.component';

@Component({
  selector: 'jhi-repayment-transfer',
  templateUrl: './repayment-transfer.component.html'
})
export class RepaymentTransferComponent implements OnInit, OnDestroy {
  @Input() repaymentfileID = 0;
  repaymentTransfers?: IRepaymentTransfer[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;

  constructor(
    protected repaymentTransferService: RepaymentTransferService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.repaymentTransferService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IRepaymentTransfer[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    if (this.repaymentfileID) {
      this.loadData(this.repaymentfileID);
    } else {
      this.activatedRoute.data.subscribe(data => {
        this.page = data.pagingParams.page;
        this.ascending = data.pagingParams.ascending;
        this.predicate = data.pagingParams.predicate;
        this.ngbPaginationPage = data.pagingParams.page;
        this.loadPage();
      });
    }
    this.registerChangeInRepaymentTransfers();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IRepaymentTransfer): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInRepaymentTransfers(): void {
    this.eventSubscriber = this.eventManager.subscribe('repaymentTransferListModification', () => this.loadPage());
  }

  delete(repaymentTransfer: IRepaymentTransfer): void {
    const modalRef = this.modalService.open(RepaymentTransferDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.repaymentTransfer = repaymentTransfer;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IRepaymentTransfer[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/repayment-transfer'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.repaymentTransfers = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  private loadData(repaymentfileID?: number): void {
    this.repaymentTransferService.findByRepaymentId(repaymentfileID).subscribe(
      (res: HttpResponse<IRepaymentTransfer[]>) => {
        this.repaymentTransfers = res.body as IRepaymentTransfer[];
      },
      () => {
        console.error('Error fetching transfers');
      }
    );
    this.repaymentTransfers = [];
  }
}
