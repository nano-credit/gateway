import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IRepaymentTransfer } from 'app/shared/model/repayment/repayment-transfer.model';

type EntityResponseType = HttpResponse<IRepaymentTransfer>;
type EntityArrayResponseType = HttpResponse<IRepaymentTransfer[]>;

@Injectable({ providedIn: 'root' })
export class RepaymentTransferService {
  public resourceUrl = SERVER_API_URL + 'services/repayment/api/repayment-transfers';

  constructor(protected http: HttpClient) {}

  create(repaymentTransfer: IRepaymentTransfer): Observable<EntityResponseType> {
    return this.http.post<IRepaymentTransfer>(this.resourceUrl, repaymentTransfer, { observe: 'response' });
  }

  update(repaymentTransfer: IRepaymentTransfer): Observable<EntityResponseType> {
    return this.http.put<IRepaymentTransfer>(this.resourceUrl, repaymentTransfer, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRepaymentTransfer>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRepaymentTransfer[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findByRepaymentId(repaymentfileID: number | undefined): Observable<EntityArrayResponseType> {
    return this.http.get<IRepaymentTransfer[]>(`${this.resourceUrl}/repaymentfile/${repaymentfileID}`, { observe: 'response' });
  }
}
