import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MicrocreditSharedModule } from 'app/shared/shared.module';
import { RepaymentTransferDetailComponent } from './repayment-transfer-detail.component';
import { RepaymentTransferUpdateComponent } from './repayment-transfer-update.component';
import { RepaymentTransferDeleteDialogComponent } from './repayment-transfer-delete-dialog.component';
import { repaymentTransferRoute } from './repayment-transfer.route';
import { RepaymentSharedModule } from 'app/entities/repayment/repayment-shared.module';

@NgModule({
  imports: [MicrocreditSharedModule, RouterModule.forChild(repaymentTransferRoute), RepaymentSharedModule],
  declarations: [RepaymentTransferDetailComponent, RepaymentTransferUpdateComponent, RepaymentTransferDeleteDialogComponent],
  entryComponents: [RepaymentTransferDeleteDialogComponent]
})
export class RepaymentRepaymentTransferModule {}
