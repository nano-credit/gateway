import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IRepaymentTransfer, RepaymentTransfer } from 'app/shared/model/repayment/repayment-transfer.model';
import { RepaymentTransferService } from './repayment-transfer.service';
import { RepaymentTransferComponent } from './repayment-transfer.component';
import { RepaymentTransferDetailComponent } from './repayment-transfer-detail.component';
import { RepaymentTransferUpdateComponent } from './repayment-transfer-update.component';

@Injectable({ providedIn: 'root' })
export class RepaymentTransferResolve implements Resolve<IRepaymentTransfer> {
  constructor(private service: RepaymentTransferService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRepaymentTransfer> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((repaymentTransfer: HttpResponse<RepaymentTransfer>) => {
          if (repaymentTransfer.body) {
            return of(repaymentTransfer.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new RepaymentTransfer());
  }
}

export const repaymentTransferRoute: Routes = [
  {
    path: '',
    component: RepaymentTransferComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'RepaymentTransfers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RepaymentTransferDetailComponent,
    resolve: {
      repaymentTransfer: RepaymentTransferResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RepaymentTransfers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RepaymentTransferUpdateComponent,
    resolve: {
      repaymentTransfer: RepaymentTransferResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RepaymentTransfers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RepaymentTransferUpdateComponent,
    resolve: {
      repaymentTransfer: RepaymentTransferResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RepaymentTransfers'
    },
    canActivate: [UserRouteAccessService]
  }
];
