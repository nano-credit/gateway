import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IRepaymentTransfer, RepaymentTransfer } from 'app/shared/model/repayment/repayment-transfer.model';
import { RepaymentTransferService } from './repayment-transfer.service';
import { IRepaymentFile } from 'app/shared/model/repayment/repayment-file.model';
import { RepaymentFileService } from 'app/entities/repayment/repayment-file/repayment-file.service';

@Component({
  selector: 'jhi-repayment-transfer-update',
  templateUrl: './repayment-transfer-update.component.html'
})
export class RepaymentTransferUpdateComponent implements OnInit {
  isSaving = false;
  repaymentfiles: IRepaymentFile[] = [];

  editForm = this.fb.group({
    id: [],
    sourceWallet: [null, [Validators.required]],
    targetWallet: [null, [Validators.required]],
    amount: [null, [Validators.required]],
    type: [],
    paymentState: [],
    paymentTxId: [],
    paymentMessage: [],
    repaymentFileId: []
  });

  constructor(
    protected repaymentTransferService: RepaymentTransferService,
    protected repaymentFileService: RepaymentFileService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ repaymentTransfer }) => {
      this.updateForm(repaymentTransfer);

      this.repaymentFileService.query().subscribe((res: HttpResponse<IRepaymentFile[]>) => (this.repaymentfiles = res.body || []));
    });
  }

  updateForm(repaymentTransfer: IRepaymentTransfer): void {
    this.editForm.patchValue({
      id: repaymentTransfer.id,
      sourceWallet: repaymentTransfer.sourceWallet,
      targetWallet: repaymentTransfer.targetWallet,
      amount: repaymentTransfer.amount,
      type: repaymentTransfer.type,
      paymentState: repaymentTransfer.paymentState,
      paymentTxId: repaymentTransfer.paymentTxId,
      paymentMessage: repaymentTransfer.paymentMessage,
      repaymentFileId: repaymentTransfer.repaymentFileId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const repaymentTransfer = this.createFromForm();
    if (repaymentTransfer.id !== undefined) {
      this.subscribeToSaveResponse(this.repaymentTransferService.update(repaymentTransfer));
    } else {
      this.subscribeToSaveResponse(this.repaymentTransferService.create(repaymentTransfer));
    }
  }

  private createFromForm(): IRepaymentTransfer {
    return {
      ...new RepaymentTransfer(),
      id: this.editForm.get(['id'])!.value,
      sourceWallet: this.editForm.get(['sourceWallet'])!.value,
      targetWallet: this.editForm.get(['targetWallet'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      type: this.editForm.get(['type'])!.value,
      paymentState: this.editForm.get(['paymentState'])!.value,
      paymentTxId: this.editForm.get(['paymentTxId'])!.value,
      paymentMessage: this.editForm.get(['paymentMessage'])!.value,
      repaymentFileId: this.editForm.get(['repaymentFileId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRepaymentTransfer>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IRepaymentFile): any {
    return item.id;
  }
}
