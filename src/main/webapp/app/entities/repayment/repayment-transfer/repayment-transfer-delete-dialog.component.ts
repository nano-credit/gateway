import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRepaymentTransfer } from 'app/shared/model/repayment/repayment-transfer.model';
import { RepaymentTransferService } from './repayment-transfer.service';

@Component({
  templateUrl: './repayment-transfer-delete-dialog.component.html'
})
export class RepaymentTransferDeleteDialogComponent {
  repaymentTransfer?: IRepaymentTransfer;

  constructor(
    protected repaymentTransferService: RepaymentTransferService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.repaymentTransferService.delete(id).subscribe(() => {
      this.eventManager.broadcast('repaymentTransferListModification');
      this.activeModal.close();
    });
  }
}
