import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRepaymentTransfer } from 'app/shared/model/repayment/repayment-transfer.model';

@Component({
  selector: 'jhi-repayment-transfer-detail',
  templateUrl: './repayment-transfer-detail.component.html'
})
export class RepaymentTransferDetailComponent implements OnInit {
  repaymentTransfer: IRepaymentTransfer | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ repaymentTransfer }) => (this.repaymentTransfer = repaymentTransfer));
  }

  previousState(): void {
    window.history.back();
  }
}
