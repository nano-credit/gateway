import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IRepaymentFile, RepaymentFile } from 'app/shared/model/repayment/repayment-file.model';
import { RepaymentFileService } from './repayment-file.service';
import { RepaymentFileComponent } from './repayment-file.component';
import { RepaymentFileDetailComponent } from './repayment-file-detail.component';
import { RepaymentFileUpdateComponent } from './repayment-file-update.component';

@Injectable({ providedIn: 'root' })
export class RepaymentFileResolve implements Resolve<IRepaymentFile> {
  constructor(private service: RepaymentFileService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRepaymentFile> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((repaymentFile: HttpResponse<RepaymentFile>) => {
          if (repaymentFile.body) {
            return of(repaymentFile.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new RepaymentFile());
  }
}

export const repaymentFileRoute: Routes = [
  {
    path: '',
    component: RepaymentFileComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,desc',
      pageTitle: 'RepaymentFiles'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RepaymentFileDetailComponent,
    resolve: {
      repaymentFile: RepaymentFileResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RepaymentFiles'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RepaymentFileUpdateComponent,
    resolve: {
      repaymentFile: RepaymentFileResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RepaymentFiles'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RepaymentFileUpdateComponent,
    resolve: {
      repaymentFile: RepaymentFileResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RepaymentFiles'
    },
    canActivate: [UserRouteAccessService]
  }
];
