import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MicrocreditSharedModule } from 'app/shared/shared.module';
import { RepaymentFileComponent } from './repayment-file.component';
import { RepaymentFileDetailComponent } from './repayment-file-detail.component';
import { RepaymentFileUpdateComponent } from './repayment-file-update.component';
import { RepaymentFileDeleteDialogComponent } from './repayment-file-delete-dialog.component';
import { repaymentFileRoute } from './repayment-file.route';
import { RepaymentSharedModule } from 'app/entities/repayment/repayment-shared.module';

@NgModule({
  imports: [MicrocreditSharedModule, RouterModule.forChild(repaymentFileRoute), RepaymentSharedModule],
  declarations: [RepaymentFileComponent, RepaymentFileDetailComponent, RepaymentFileUpdateComponent, RepaymentFileDeleteDialogComponent],
  entryComponents: [RepaymentFileDeleteDialogComponent]
})
export class RepaymentRepaymentFileModule {}
