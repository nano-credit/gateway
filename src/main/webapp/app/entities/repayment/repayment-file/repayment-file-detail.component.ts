import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {IRepaymentFile} from 'app/shared/model/repayment/repayment-file.model';

@Component({
  selector: 'jhi-repayment-file-detail',
  templateUrl: './repayment-file-detail.component.html'
})
export class RepaymentFileDetailComponent implements OnInit {
  repaymentFile: IRepaymentFile | null = null;
  enCours = 0;

  constructor(protected activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({repaymentFile}) => {
      this.repaymentFile = repaymentFile;
      if (this.repaymentFile) {
        const capital = this.repaymentFile.capitalOutstanding || 0;
        const fdd = this.repaymentFile.feesOutstanding || 0;
        const pr1 = this.repaymentFile.pr1Outstanding || 0;
        const pr2 = this.repaymentFile.pr2Outstanding || 0;
        this.enCours = capital + fdd + pr1 + pr2;
      }
    });
  }

  previousState(): void {
    window.history.back();
  }
}
