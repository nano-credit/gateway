import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IRepaymentFile, RepaymentFile } from 'app/shared/model/repayment/repayment-file.model';
import { RepaymentFileService } from './repayment-file.service';

@Component({
  selector: 'jhi-repayment-file-update',
  templateUrl: './repayment-file-update.component.html'
})
export class RepaymentFileUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    loanFileID: [null, [Validators.required]],
    initialCapital: [null, [Validators.required]],
    initialDueDate: [null, [Validators.required]],
    applicationFees: [null, [Validators.required]],
    pr1Fees: [],
    pr2Fees: [],
    capitalOutstanding: [],
    feesOutstanding: [],
    pr1Outstanding: [],
    pr2Outstanding: [],
    repaymentState: [null, [Validators.required]],
    repaymentStatus: [null, [Validators.required]],
    dueDate: [null, [Validators.required]]
  });

  constructor(protected repaymentFileService: RepaymentFileService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ repaymentFile }) => {
      if (!repaymentFile.id) {
        const today = moment().startOf('day');
        repaymentFile.initialDueDate = today;
        repaymentFile.dueDate = today;
      }

      this.updateForm(repaymentFile);
    });
  }

  updateForm(repaymentFile: IRepaymentFile): void {
    this.editForm.patchValue({
      id: repaymentFile.id,
      loanFileID: repaymentFile.loanFileID,
      initialCapital: repaymentFile.initialCapital,
      initialDueDate: repaymentFile.initialDueDate ? repaymentFile.initialDueDate.format(DATE_TIME_FORMAT) : null,
      applicationFees: repaymentFile.applicationFees,
      pr1Fees: repaymentFile.pr1Fees,
      pr2Fees: repaymentFile.pr2Fees,
      capitalOutstanding: repaymentFile.capitalOutstanding,
      feesOutstanding: repaymentFile.feesOutstanding,
      pr1Outstanding: repaymentFile.pr1Outstanding,
      pr2Outstanding: repaymentFile.pr2Outstanding,
      repaymentState: repaymentFile.repaymentState,
      repaymentStatus: repaymentFile.repaymentStatus,
      dueDate: repaymentFile.dueDate ? repaymentFile.dueDate.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const repaymentFile = this.createFromForm();
    if (repaymentFile.id !== undefined) {
      this.subscribeToSaveResponse(this.repaymentFileService.update(repaymentFile));
    } else {
      this.subscribeToSaveResponse(this.repaymentFileService.create(repaymentFile));
    }
  }

  private createFromForm(): IRepaymentFile {
    return {
      ...new RepaymentFile(),
      id: this.editForm.get(['id'])!.value,
      loanFileID: this.editForm.get(['loanFileID'])!.value,
      initialCapital: this.editForm.get(['initialCapital'])!.value,
      initialDueDate: this.editForm.get(['initialDueDate'])!.value
        ? moment(this.editForm.get(['initialDueDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      applicationFees: this.editForm.get(['applicationFees'])!.value,
      pr1Fees: this.editForm.get(['pr1Fees'])!.value,
      pr2Fees: this.editForm.get(['pr2Fees'])!.value,
      capitalOutstanding: this.editForm.get(['capitalOutstanding'])!.value,
      feesOutstanding: this.editForm.get(['feesOutstanding'])!.value,
      pr1Outstanding: this.editForm.get(['pr1Outstanding'])!.value,
      pr2Outstanding: this.editForm.get(['pr2Outstanding'])!.value,
      repaymentState: this.editForm.get(['repaymentState'])!.value,
      repaymentStatus: this.editForm.get(['repaymentStatus'])!.value,
      dueDate: this.editForm.get(['dueDate'])!.value ? moment(this.editForm.get(['dueDate'])!.value, DATE_TIME_FORMAT) : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRepaymentFile>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
