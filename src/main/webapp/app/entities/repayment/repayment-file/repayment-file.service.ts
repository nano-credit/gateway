import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IRepaymentFile } from 'app/shared/model/repayment/repayment-file.model';

type EntityResponseType = HttpResponse<IRepaymentFile>;
type EntityArrayResponseType = HttpResponse<IRepaymentFile[]>;

@Injectable({ providedIn: 'root' })
export class RepaymentFileService {
  public resourceUrl = SERVER_API_URL + 'services/repayment/api/repayment-files';

  constructor(protected http: HttpClient) {}

  create(repaymentFile: IRepaymentFile): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(repaymentFile);
    return this.http
      .post<IRepaymentFile>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(repaymentFile: IRepaymentFile): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(repaymentFile);
    return this.http
      .put<IRepaymentFile>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IRepaymentFile>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IRepaymentFile[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(repaymentFile: IRepaymentFile): IRepaymentFile {
    const copy: IRepaymentFile = Object.assign({}, repaymentFile, {
      initialDueDate:
        repaymentFile.initialDueDate && repaymentFile.initialDueDate.isValid() ? repaymentFile.initialDueDate.toJSON() : undefined,
      dueDate: repaymentFile.dueDate && repaymentFile.dueDate.isValid() ? repaymentFile.dueDate.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.initialDueDate = res.body.initialDueDate ? moment(res.body.initialDueDate) : undefined;
      res.body.dueDate = res.body.dueDate ? moment(res.body.dueDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((repaymentFile: IRepaymentFile) => {
        repaymentFile.initialDueDate = repaymentFile.initialDueDate ? moment(repaymentFile.initialDueDate) : undefined;
        repaymentFile.dueDate = repaymentFile.dueDate ? moment(repaymentFile.dueDate) : undefined;
      });
    }
    return res;
  }

  search(req: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRepaymentFile[]>(`${this.resourceUrl}/search`, { params: options, observe: 'response' });
  }
}
