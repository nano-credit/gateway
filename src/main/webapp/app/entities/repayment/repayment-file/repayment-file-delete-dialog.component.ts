import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRepaymentFile } from 'app/shared/model/repayment/repayment-file.model';
import { RepaymentFileService } from './repayment-file.service';

@Component({
  templateUrl: './repayment-file-delete-dialog.component.html'
})
export class RepaymentFileDeleteDialogComponent {
  repaymentFile?: IRepaymentFile;

  constructor(
    protected repaymentFileService: RepaymentFileService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.repaymentFileService.delete(id).subscribe(() => {
      this.eventManager.broadcast('repaymentFileListModification');
      this.activeModal.close();
    });
  }
}
