import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IRepaymentFile } from 'app/shared/model/repayment/repayment-file.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { RepaymentFileService } from './repayment-file.service';
import { RepaymentFileDeleteDialogComponent } from './repayment-file-delete-dialog.component';
import { RepaymentState } from 'app/shared/model/enumerations/repayment-state.model';
import { RepaymentStatus } from 'app/shared/model/enumerations/repayment-status.model';

@Component({
  selector: 'jhi-repayment-file',
  templateUrl: './repayment-file.component.html'
})
export class RepaymentFileComponent implements OnInit, OnDestroy {
  repaymentFiles?: IRepaymentFile[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  msisdn?: string;
  debut?: string;
  fin?: string;
  repaymentState?: RepaymentState;
  repaymentStatus?: RepaymentStatus;

  constructor(
    protected repaymentFileService: RepaymentFileService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.repaymentFileService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IRepaymentFile[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  initializeSearch(): void {
    this.msisdn = '';
    this.repaymentState = undefined;
    this.repaymentStatus = undefined;
    this.debut = '';
    this.fin = '';
    this.loadPage(1);
  }

  loadPageSearch(page?: number): void {
    const pageToLoad: number = page || this.page;
    // eslint-disable-next-line no-console
    console.log('dates');
    // eslint-disable-next-line no-console
    console.log(this.debut);
    // eslint-disable-next-line no-console
    console.log(this.fin);
    this.repaymentFileService
      .search({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
        msisdn: this.msisdn || '',
        state: this.repaymentState || '',
        status: this.repaymentStatus || '',
        debut: this.debut !== undefined && this.debut !== '' ? new Date(this.debut).toISOString() : '',
        fin: this.fin !== undefined && this.fin !== '' ? new Date(this.fin).toISOString() : ''
      })
      .subscribe(
        (res: HttpResponse<IRepaymentFile[]>) => this.onSuccess(res.body, res.headers, 1),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInRepaymentFiles();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IRepaymentFile): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInRepaymentFiles(): void {
    this.eventSubscriber = this.eventManager.subscribe('repaymentFileListModification', () => this.loadPage());
  }

  delete(repaymentFile: IRepaymentFile): void {
    const modalRef = this.modalService.open(RepaymentFileDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.repaymentFile = repaymentFile;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IRepaymentFile[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/repayment-file'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.repaymentFiles = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }
}
