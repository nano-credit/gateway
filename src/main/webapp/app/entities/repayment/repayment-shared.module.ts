import { NgModule } from '@angular/core';
import { RepaymentTransferComponent } from 'app/entities/repayment/repayment-transfer/repayment-transfer.component';
import { MicrocreditSharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [MicrocreditSharedModule, RouterModule],
  declarations: [RepaymentTransferComponent],
  exports: [RepaymentTransferComponent]
})
export class RepaymentSharedModule {}
