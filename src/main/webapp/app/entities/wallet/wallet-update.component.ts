import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IWallet, Wallet } from 'app/shared/model/wallet.model';
import { WalletService } from './wallet.service';

@Component({
  selector: 'jhi-wallet-update',
  templateUrl: './wallet-update.component.html'
})
export class WalletUpdateComponent implements OnInit {
  isSaving = false;
  showSpares = false;
  check = false;
  match = false;

  editForm = this.fb.group({
    id: [],
    msisdn: [null, [Validators.required]],
    pin: [null, [Validators.required]],
    confirmPin: [null, [Validators.required]],
    wallet_name: [],
    wallet_type: [],
    wallet_min_balance: [],
    wallet_max_balance: [],
    email_alerts_recipients_to: [],
    email_alerts_recipients_cc: [],
    email_min_wallet_alert_template: [],
    email_max_wallet_alert_template: [],
    sms_alert_recipients: [],
    sms_min_wallet_alert_template: [],
    sms_max_wallet_alert_template: [],
    spare1: [],
    spare2: [],
    spare3: [],
    spare4: [],
    spare5: []
  });

  confirmPin = '';

  constructor(protected walletService: WalletService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ wallet }) => {
      this.updateForm(wallet);
      this.showSpares = wallet.id ? true : false;
      // eslint-disable-next-line no-console
    });
  }

  updateForm(wallet: IWallet): void {
    this.editForm.patchValue({
      id: wallet.id,
      msisdn: wallet.msisdn,
      pin: wallet.pin,
      wallet_name: wallet.wallet_name,
      wallet_type: wallet.wallet_type,
      wallet_min_balance: wallet.wallet_min_balance,
      wallet_max_balance: wallet.wallet_max_balance,
      email_alerts_recipients_to: wallet.email_alerts_recipients_to,
      email_alerts_recipients_cc: wallet.email_alerts_recipients_cc,
      email_min_wallet_alert_template: wallet.email_min_wallet_alert_template,
      email_max_wallet_alert_template: wallet.email_max_wallet_alert_template,
      sms_alert_recipients: wallet.sms_alert_recipients,
      sms_min_wallet_alert_template: wallet.sms_min_wallet_alert_template,
      sms_max_wallet_alert_template: wallet.sms_max_wallet_alert_template,
      spare1: wallet.spare1,
      spare2: wallet.spare2,
      spare3: wallet.spare3,
      spare4: wallet.spare4,
      spare5: wallet.spare5
    });
  }

  checkPassword(): void {
    this.check = true;
    // eslint-disable-next-line no-console
    console.log('pin');
    // eslint-disable-next-line no-console
    console.log(this.editForm.get('pin')!.value);
    // eslint-disable-next-line no-console
    console.log('confirmpin');
    // eslint-disable-next-line no-console
    console.log(this.confirmPin);
    this.match = this.editForm.get('pin')!.value === this.confirmPin ? true : false;
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const wallet = this.createFromForm();
    if (wallet.id !== undefined) {
      this.subscribeToSaveResponse(this.walletService.update(wallet));
    } else {
      this.subscribeToSaveResponse(this.walletService.create(wallet));
    }
  }

  private createFromForm(): IWallet {
    return {
      ...new Wallet(),
      id: this.editForm.get(['id'])!.value,
      msisdn: this.editForm.get(['msisdn'])!.value,
      pin: this.editForm.get(['pin'])!.value,
      wallet_name: this.editForm.get(['wallet_name'])!.value,
      wallet_type: this.editForm.get(['wallet_type'])!.value,
      wallet_min_balance: this.editForm.get(['wallet_min_balance'])!.value,
      wallet_max_balance: this.editForm.get(['wallet_max_balance'])!.value,
      email_alerts_recipients_to: this.editForm.get(['email_alerts_recipients_to'])!.value,
      email_alerts_recipients_cc: this.editForm.get(['email_alerts_recipients_cc'])!.value,
      email_min_wallet_alert_template: this.editForm.get(['email_min_wallet_alert_template'])!.value,
      email_max_wallet_alert_template: this.editForm.get(['email_max_wallet_alert_template'])!.value,
      sms_alert_recipients: this.editForm.get(['sms_alert_recipients'])!.value,
      sms_min_wallet_alert_template: this.editForm.get(['sms_min_wallet_alert_template'])!.value,
      sms_max_wallet_alert_template: this.editForm.get(['sms_max_wallet_alert_template'])!.value,
      spare1: this.editForm.get(['spare1'])!.value,
      spare2: this.editForm.get(['spare2'])!.value,
      spare3: this.editForm.get(['spare3'])!.value,
      spare4: this.editForm.get(['spare4'])!.value,
      spare5: this.editForm.get(['spare5'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWallet>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
