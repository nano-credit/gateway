import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'loan-file',
        loadChildren: () => import('./loan/loan-file/loan-file.module').then(m => m.LoanLoanFileModule)
      },
      {
        path: 'ume-transfer',
        loadChildren: () => import('./loan/ume-transfer/ume-transfer.module').then(m => m.LoanUMETransferModule)
      },
      {
        path: 'offer',
        loadChildren: () => import('./offer/offer.module').then(m => m.MicrocreditOfferModule)
      },
      {
        path: 'wallet',
        loadChildren: () => import('./wallet/wallet.module').then(m => m.MicrocreditWalletModule)
      },
      {
        path: 'repayment-file',
        loadChildren: () => import('./repayment/repayment-file/repayment-file.module').then(m => m.RepaymentRepaymentFileModule)
      },
      {
        path: 'repayment-transfer',
        loadChildren: () => import('./repayment/repayment-transfer/repayment-transfer.module').then(m => m.RepaymentRepaymentTransferModule)
      },
      {
        path: 'access-list',
        loadChildren: () => import('./access-list/access-list.module').then(m => m.MicrocreditAccessListModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class MicrocreditEntityModule {}
