import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { MicrocreditSharedModule } from 'app/shared/shared.module';
import { MicrocreditCoreModule } from 'app/core/core.module';
import { MicrocreditAppRoutingModule } from './app-routing.module';
import { MicrocreditHomeModule } from './home/home.module';
import { MicrocreditEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    MicrocreditSharedModule,
    MicrocreditCoreModule,
    MicrocreditHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    MicrocreditEntityModule,
    MicrocreditAppRoutingModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent]
})
export class MicrocreditAppModule {}
