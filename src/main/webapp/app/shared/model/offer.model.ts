import { Wallet } from 'app/shared/model/wallet.model';

export interface IOffer {
  id?: number;
  tenantId?: string;
  offerName?: string;
  activated?: boolean;
  freeSeniority?: number;
  freemoneySeniority?: number;
  defaultLoanDuration?: number;
  minCustAge?: number;
  maxCustAge?: number;
  maxSimultaneousLoan?: number;
  maxLoanPerDate?: number;
  maxCurrentBalance?: number;
  minAmount?: number;
  maxAmount?: number;
  totalFeesRate?: number;
  minRepayment?: number;
  pr0Rate?: number;
  pr1Rate?: number;
  pr2Rate?: number;
  msisdnAdmin?: string;
  emailAdmin?: string;
  loanWalletId?: number;
  loanWallet?: Wallet;
  repaymentWalletId?: number;
  repaymentWallet?: Wallet;
  feesWalletId?: number;
  feesWallet?: Wallet;

  feesFreeMoneyRate?: number;
  feesBankRate?: number;
  nbJourPr1?: number;
  nbJourPr2?: number;
  rappelPr0?: number;
  rappelPr1?: number;
  rappelPr2?: number;
  maxS1?: number;
  maxS2?: number;
  maxS3?: number;
  maxS4?: number;
}

export class Offer implements IOffer {
  constructor(
    public id?: number,
    public tenantId?: string,
    public offerName?: string,
    public activated?: boolean,
    public freeSeniority?: number,
    public freemoneySeniority?: number,
    public defaultLoanDuration?: number,
    public minCustAge?: number,
    public maxCustAge?: number,
    public maxSimultaneousLoan?: number,
    public maxLoanPerDate?: number,
    public maxCurrentBalance?: number,
    public minAmount?: number,
    public maxAmount?: number,
    public totalFeesRate?: number,
    public minRepayment?: number,
    public pr0Rate?: number,
    public pr1Rate?: number,
    public pr2Rate?: number,
    public msisdnAdmin?: string,
    public emailAdmin?: string,
    public loanWalletId?: number,
    public loanWallet?: Wallet,
    public repaymentWalletId?: number,
    public repaymentWallet?: Wallet,
    public feesWalletId?: number,
    public feesWallet?: Wallet,
    public feesFreeMoneyRate?: number,
    public feesBankRate?: number,
    public nbJourPr1?: number,
    public nbJourPr2?: number,
    public rappelPr0?: number,
    public rappelPr1?: number,
    public rappelPr2?: number,
    public maxS1?: number,
    public maxS2?: number,
    public maxS3?: number,
    public maxS4?: number
  ) {
    this.activated = this.activated || false;
  }
}
