import { AccessStatus } from 'app/shared/model/enumerations/access-status.model';

export interface IAccessList {
  id?: number;
  msisdn?: string;
  cin?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  status?: AccessStatus;
}

export class AccessList implements IAccessList {
  constructor(
    public id?: number,
    public msisdn?: string,
    public cin?: string,
    public firstName?: string,
    public lastName?: string,
    public email?: string,
    public status?: AccessStatus
  ) {}
}
