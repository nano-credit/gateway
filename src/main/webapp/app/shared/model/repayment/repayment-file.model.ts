import { Moment } from 'moment';
import { IRepaymentTransfer } from 'app/shared/model/repayment/repayment-transfer.model';
import { RepaymentState } from 'app/shared/model/enumerations/repayment-state.model';
import { RepaymentStatus } from 'app/shared/model/enumerations/repayment-status.model';

export interface IRepaymentFile {
  id?: number;
  loanFileID?: number;
  initialCapital?: number;
  initialDueDate?: Moment;
  applicationFees?: number;
  pr1Fees?: number;
  pr2Fees?: number;
  capitalOutstanding?: number;
  feesOutstanding?: number;
  pr1Outstanding?: number;
  pr2Outstanding?: number;
  repaymentState?: RepaymentState;
  repaymentStatus?: RepaymentStatus;
  dueDate?: Moment;

  msisdn?: string;
  offerSlug?: string;
  defaultLoanDuration?: number;
  nbJourPr1?: number;
  nbJourPr2?: number;
  pr0Rate?: number;
  pr1Rate?: number;
  pr2Rate?: number;
  rappelPr0?: string;
  rappelPr1?: string;
  rappelPr2?: string;
  createdBy?: string;
  createdDate?: Moment;
  lastModifiedBy?: string;
  lastModifiedDate?: Moment;
  transfers?: IRepaymentTransfer[];
}

export class RepaymentFile implements IRepaymentFile {
  constructor(
    public id?: number,
    public loanFileID?: number,
    public initialCapital?: number,
    public initialDueDate?: Moment,
    public applicationFees?: number,
    public pr1Fees?: number,
    public pr2Fees?: number,
    public capitalOutstanding?: number,
    public feesOutstanding?: number,
    public pr1Outstanding?: number,
    public pr2Outstanding?: number,
    public repaymentState?: RepaymentState,
    public repaymentStatus?: RepaymentStatus,
    public dueDate?: Moment,
    public msisdn?: string,
    public offerSlug?: string,
    public defaultLoanDuration?: number,
    public nbJourPr1?: number,
    public nbJourPr2?: number,
    public pr0Rate?: number,
    public pr1Rate?: number,
    public pr2Rate?: number,
    public rappelPr0?: string,
    public rappelPr1?: string,
    public rappelPr2?: string,
    public transfers?: IRepaymentTransfer[],
    public createdBy?: string,
    public createdDate?: Moment,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Moment
  ) {}
}
