import { RepaymentTransferType } from 'app/shared/model/enumerations/repayment-transfer-type.model';
import { PaymentState } from 'app/shared/model/enumerations/payment-state.model';
import { Moment } from 'moment';

export interface IRepaymentTransfer {
  id?: number;
  sourceWallet?: string;
  targetWallet?: string;
  amount?: number;
  type?: RepaymentTransferType;
  paymentState?: PaymentState;
  paymentTxId?: string;
  paymentMessage?: string;
  repaymentFileId?: number;
  createdBy?: string;
  createdDate?: Moment;
  lastModifiedBy?: string;
  lastModifiedDate?: Moment;
}

export class RepaymentTransfer implements IRepaymentTransfer {
  constructor(
    public id?: number,
    public sourceWallet?: string,
    public targetWallet?: string,
    public amount?: number,
    public type?: RepaymentTransferType,
    public paymentState?: PaymentState,
    public paymentTxId?: string,
    public paymentMessage?: string,
    public repaymentFileId?: number,
    public createdBy?: string,
    public createdMoment?: Moment,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Moment
  ) {}
}
