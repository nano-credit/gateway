export interface IWallet {
  id?: number;
  msisdn?: string;
  pin?: string;
  wallet_name?: string;
  wallet_type?: string;
  wallet_min_balance?: number;
  wallet_max_balance?: number;
  email_alerts_recipients_to?: string;
  email_alerts_recipients_cc?: string;
  email_min_wallet_alert_template?: string;
  email_max_wallet_alert_template?: string;
  sms_alert_recipients?: string;
  sms_min_wallet_alert_template?: string;
  sms_max_wallet_alert_template?: string;
  spare1?: string;
  spare2?: string;
  spare3?: string;
  spare4?: string;
  spare5?: string;
}

export class Wallet implements IWallet {
  constructor(
    public id?: number,
    public msisdn?: string,
    public pin?: string,
    public wallet_name?: string,
    public wallet_type?: string,
    public wallet_min_balance?: number,
    public wallet_max_balance?: number,
    public email_alerts_recipients_to?: string,
    public email_alerts_recipients_cc?: string,
    public email_min_wallet_alert_template?: string,
    public email_max_wallet_alert_template?: string,
    public sms_alert_recipients?: string,
    public sms_min_wallet_alert_template?: string,
    public sms_max_wallet_alert_template?: string,
    public spare1?: string,
    public spare2?: string,
    public spare3?: string,
    public spare4?: string,
    public spare5?: string
  ) {}
}
