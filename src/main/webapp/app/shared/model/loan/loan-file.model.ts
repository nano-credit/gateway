import { IUMETransfer } from 'app/shared/model/loan/ume-transfer.model';
import { IDType } from 'app/shared/model/enumerations/id-type.model';
import { GrantingStatus } from 'app/shared/model/enumerations/granting-status.model';
import { Moment } from 'moment';

export interface ILoanFile {
  id?: number;
  msisdn?: string;
  amount?: number;
  offerSlug?: string;
  balance?: number;
  idType?: IDType;
  idNumber?: string;
  eligibilityScore?: number;
  grantingStatus?: GrantingStatus;
  fees?: number;
  grantedAmount?: number;
  grantingMessage?: string;
  grantingProcessMessage?: string;
  repaymentId?: number;
  repaymentLog?: string;
  transfers?: IUMETransfer[];
  createdBy?: string;
  createdDate?: Moment;
  lastModifiedBy?: string;
  lastModifiedDate?: Moment;
}

export class LoanFile implements ILoanFile {
  constructor(
    public id?: number,
    public msisdn?: string,
    public amount?: number,
    public offerSlug?: string,
    public balance?: number,
    public idType?: IDType,
    public idNumber?: string,
    public eligibilityScore?: number,
    public grantingStatus?: GrantingStatus,
    public fees?: number,
    public grantedAmount?: number,
    public grantingMessage?: string,
    public grantingProcessMessage?: string,
    public repaymentId?: number,
    public repaymentLog?: string,
    public transfers?: IUMETransfer[],
    public createdBy?: string,
    public createdDate?: Moment,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Moment
  ) {}
}
