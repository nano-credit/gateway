import { TransferType } from 'app/shared/model/enumerations/transfer-type.model';
import { PaymentState } from 'app/shared/model/enumerations/payment-state.model';
import { Moment } from 'moment';

export interface IUMETransfer {
  id?: number;
  sourceWallet?: string;
  targetWallet?: string;
  amount?: number;
  type?: TransferType;
  paymentState?: PaymentState;
  paymentTxId?: string;
  paymentMessage?: string;
  loanFileId?: number;
  createdBy?: string;
  createdDate?: Moment;
  lastModifiedBy?: string;
  lastModifiedDate?: Moment;
}

export class UMETransfer implements IUMETransfer {
  constructor(
    public id?: number,
    public sourceWallet?: string,
    public targetWallet?: string,
    public amount?: number,
    public type?: TransferType,
    public paymentState?: PaymentState,
    public paymentTxId?: string,
    public paymentMessage?: string,
    public loanFileId?: number,
    public createdBy?: string,
    public createdDate?: Moment,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Moment
  ) {}
}
