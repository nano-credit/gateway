export const enum RepaymentStatus {
  NA,
  IN_ADVANCE,
  TERM,
  UTIMATELY,
  PR1,
  PR2,
  RECOVERED,
  OVERDUE
}
