export const enum PaymentState {
  PENDING,
  SUCCESS,
  FAILED,
  ROLLEDBACK,
  ROLLED_BACK_W_ERROR
}
