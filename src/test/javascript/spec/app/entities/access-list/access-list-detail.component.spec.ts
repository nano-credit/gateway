import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MicrocreditTestModule } from '../../../test.module';
import { AccessListDetailComponent } from 'app/entities/access-list/access-list-detail.component';
import { AccessList } from 'app/shared/model/access-list.model';

describe('Component Tests', () => {
  describe('AccessList Management Detail Component', () => {
    let comp: AccessListDetailComponent;
    let fixture: ComponentFixture<AccessListDetailComponent>;
    const route = ({ data: of({ accessList: new AccessList(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MicrocreditTestModule],
        declarations: [AccessListDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(AccessListDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AccessListDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load accessList on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.accessList).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
