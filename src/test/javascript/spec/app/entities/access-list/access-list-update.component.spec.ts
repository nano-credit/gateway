import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MicrocreditTestModule } from '../../../test.module';
import { AccessListUpdateComponent } from 'app/entities/access-list/access-list-update.component';
import { AccessListService } from 'app/entities/access-list/access-list.service';
import { AccessList } from 'app/shared/model/access-list.model';

describe('Component Tests', () => {
  describe('AccessList Management Update Component', () => {
    let comp: AccessListUpdateComponent;
    let fixture: ComponentFixture<AccessListUpdateComponent>;
    let service: AccessListService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MicrocreditTestModule],
        declarations: [AccessListUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(AccessListUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AccessListUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AccessListService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AccessList(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AccessList();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
