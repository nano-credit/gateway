import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { OfferService } from 'app/entities/offer/offer.service';
import { IOffer, Offer } from 'app/shared/model/offer.model';

describe('Service Tests', () => {
  describe('Offer Service', () => {
    let injector: TestBed;
    let service: OfferService;
    let httpMock: HttpTestingController;
    let elemDefault: IOffer;
    let expectedResult: IOffer | IOffer[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(OfferService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new Offer(0, 'AAAAAAA', 'AAAAAAA', false, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Offer', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Offer()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Offer', () => {
        const returnedFromService = Object.assign(
          {
            tenantId: 'BBBBBB',
            offerName: 'BBBBBB',
            activated: true,
            freeSeniority: 1,
            freemoneySeniority: 1,
            defaultLoanDuration: 1,
            minCustAge: 1,
            maxCustAge: 1,
            maxSimultaneousLoan: 1,
            maxLoanPerDate: 1,
            maxCurrentBalance: 1,
            minAmount: 1,
            maxAmount: 1,
            totalFeesRate: 1,
            minRepayment: 1,
            pr0Rate: 1,
            pr1Rate: 1,
            pr2Rate: 1,
            msisdnAdmin: 'BBBBBB',
            emailAdmin: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Offer', () => {
        const returnedFromService = Object.assign(
          {
            tenantId: 'BBBBBB',
            offerName: 'BBBBBB',
            activated: true,
            freeSeniority: 1,
            freemoneySeniority: 1,
            defaultLoanDuration: 1,
            minCustAge: 1,
            maxCustAge: 1,
            maxSimultaneousLoan: 1,
            maxLoanPerDate: 1,
            maxCurrentBalance: 1,
            minAmount: 1,
            maxAmount: 1,
            totalFeesRate: 1,
            minRepayment: 1,
            pr0Rate: 1,
            pr1Rate: 1,
            pr2Rate: 1,
            msisdnAdmin: 'BBBBBB',
            emailAdmin: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Offer', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
