import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MicrocreditTestModule } from '../../../../test.module';
import { LoanFileDetailComponent } from 'app/entities/loan/loan-file/loan-file-detail.component';
import { LoanFile } from 'app/shared/model/loan/loan-file.model';

describe('Component Tests', () => {
  describe('LoanFile Management Detail Component', () => {
    let comp: LoanFileDetailComponent;
    let fixture: ComponentFixture<LoanFileDetailComponent>;
    const route = ({ data: of({ loanFile: new LoanFile(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MicrocreditTestModule],
        declarations: [LoanFileDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(LoanFileDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(LoanFileDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load loanFile on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.loanFile).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
