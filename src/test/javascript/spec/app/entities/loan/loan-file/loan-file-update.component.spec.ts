import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MicrocreditTestModule } from '../../../../test.module';
import { LoanFileUpdateComponent } from 'app/entities/loan/loan-file/loan-file-update.component';
import { LoanFileService } from 'app/entities/loan/loan-file/loan-file.service';
import { LoanFile } from 'app/shared/model/loan/loan-file.model';

describe('Component Tests', () => {
  describe('LoanFile Management Update Component', () => {
    let comp: LoanFileUpdateComponent;
    let fixture: ComponentFixture<LoanFileUpdateComponent>;
    let service: LoanFileService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MicrocreditTestModule],
        declarations: [LoanFileUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(LoanFileUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(LoanFileUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(LoanFileService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new LoanFile(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new LoanFile();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
