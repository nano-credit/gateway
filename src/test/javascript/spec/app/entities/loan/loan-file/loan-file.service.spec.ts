import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { LoanFileService } from 'app/entities/loan/loan-file/loan-file.service';
import { ILoanFile, LoanFile } from 'app/shared/model/loan/loan-file.model';
import { IDType } from 'app/shared/model/enumerations/id-type.model';
import { GrantingStatus } from 'app/shared/model/enumerations/granting-status.model';

describe('Service Tests', () => {
  describe('LoanFile Service', () => {
    let injector: TestBed;
    let service: LoanFileService;
    let httpMock: HttpTestingController;
    let elemDefault: ILoanFile;
    let expectedResult: ILoanFile | ILoanFile[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(LoanFileService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new LoanFile(0, 'AAAAAAA', 0, 'AAAAAAA', 0, IDType.CNI, 'AAAAAAA', 0, GrantingStatus.GRANTED, 0, 0);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a LoanFile', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new LoanFile()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a LoanFile', () => {
        const returnedFromService = Object.assign(
          {
            msisdn: 'BBBBBB',
            amount: 1,
            offerSlug: 'BBBBBB',
            balance: 1,
            idType: 'BBBBBB',
            idNumber: 'BBBBBB',
            eligibilityScore: 1,
            grantingStatus: 'BBBBBB',
            fees: 1,
            grantedAmount: 1
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of LoanFile', () => {
        const returnedFromService = Object.assign(
          {
            msisdn: 'BBBBBB',
            amount: 1,
            offerSlug: 'BBBBBB',
            balance: 1,
            idType: 'BBBBBB',
            idNumber: 'BBBBBB',
            eligibilityScore: 1,
            grantingStatus: 'BBBBBB',
            fees: 1,
            grantedAmount: 1
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a LoanFile', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
