import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MicrocreditTestModule } from '../../../../test.module';
import { UMETransferUpdateComponent } from 'app/entities/loan/ume-transfer/ume-transfer-update.component';
import { UMETransferService } from 'app/entities/loan/ume-transfer/ume-transfer.service';
import { UMETransfer } from 'app/shared/model/loan/ume-transfer.model';

describe('Component Tests', () => {
  describe('UMETransfer Management Update Component', () => {
    let comp: UMETransferUpdateComponent;
    let fixture: ComponentFixture<UMETransferUpdateComponent>;
    let service: UMETransferService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MicrocreditTestModule],
        declarations: [UMETransferUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(UMETransferUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UMETransferUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UMETransferService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new UMETransfer(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new UMETransfer();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
