import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MicrocreditTestModule } from '../../../../test.module';
import { UMETransferDetailComponent } from 'app/entities/loan/ume-transfer/ume-transfer-detail.component';
import { UMETransfer } from 'app/shared/model/loan/ume-transfer.model';

describe('Component Tests', () => {
  describe('UMETransfer Management Detail Component', () => {
    let comp: UMETransferDetailComponent;
    let fixture: ComponentFixture<UMETransferDetailComponent>;
    const route = ({ data: of({ uMETransfer: new UMETransfer(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MicrocreditTestModule],
        declarations: [UMETransferDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(UMETransferDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UMETransferDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load uMETransfer on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.uMETransfer).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
