import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { UMETransferService } from 'app/entities/loan/ume-transfer/ume-transfer.service';
import { IUMETransfer, UMETransfer } from 'app/shared/model/loan/ume-transfer.model';
import { TransferType } from 'app/shared/model/enumerations/transfer-type.model';
import { PaymentState } from 'app/shared/model/enumerations/payment-state.model';

describe('Service Tests', () => {
  describe('UMETransfer Service', () => {
    let injector: TestBed;
    let service: UMETransferService;
    let httpMock: HttpTestingController;
    let elemDefault: IUMETransfer;
    let expectedResult: IUMETransfer | IUMETransfer[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(UMETransferService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new UMETransfer(0, 'AAAAAAA', 'AAAAAAA', 0, TransferType.FEES_TRANSFER, PaymentState.PENDING, 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a UMETransfer', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new UMETransfer()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a UMETransfer', () => {
        const returnedFromService = Object.assign(
          {
            sourceWallet: 'BBBBBB',
            targetWallet: 'BBBBBB',
            amount: 1,
            type: 'BBBBBB',
            paymentState: 'BBBBBB',
            paymentTxId: 'BBBBBB',
            paymentMessage: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of UMETransfer', () => {
        const returnedFromService = Object.assign(
          {
            sourceWallet: 'BBBBBB',
            targetWallet: 'BBBBBB',
            amount: 1,
            type: 'BBBBBB',
            paymentState: 'BBBBBB',
            paymentTxId: 'BBBBBB',
            paymentMessage: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a UMETransfer', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
