import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { WalletService } from 'app/entities/wallet/wallet.service';
import { IWallet, Wallet } from 'app/shared/model/wallet.model';

describe('Service Tests', () => {
  describe('Wallet Service', () => {
    let injector: TestBed;
    let service: WalletService;
    let httpMock: HttpTestingController;
    let elemDefault: IWallet;
    let expectedResult: IWallet | IWallet[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(WalletService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new Wallet(
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        0,
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Wallet', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Wallet()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Wallet', () => {
        const returnedFromService = Object.assign(
          {
            msisdn: 'BBBBBB',
            pin: 'BBBBBB',
            wallet_name: 'BBBBBB',
            wallet_type: 'BBBBBB',
            wallet_min_balance: 1,
            wallet_max_balance: 1,
            email_alerts_recipients_to: 'BBBBBB',
            email_alerts_recipients_cc: 'BBBBBB',
            email_min_wallet_alert_template: 'BBBBBB',
            email_max_wallet_alert_template: 'BBBBBB',
            sms_alert_recipients: 'BBBBBB',
            sms_min_wallet_alert_template: 'BBBBBB',
            sms_max_wallet_alert_template: 'BBBBBB',
            spare1: 'BBBBBB',
            spare2: 'BBBBBB',
            spare3: 'BBBBBB',
            spare4: 'BBBBBB',
            spare5: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Wallet', () => {
        const returnedFromService = Object.assign(
          {
            msisdn: 'BBBBBB',
            pin: 'BBBBBB',
            wallet_name: 'BBBBBB',
            wallet_type: 'BBBBBB',
            wallet_min_balance: 1,
            wallet_max_balance: 1,
            email_alerts_recipients_to: 'BBBBBB',
            email_alerts_recipients_cc: 'BBBBBB',
            email_min_wallet_alert_template: 'BBBBBB',
            email_max_wallet_alert_template: 'BBBBBB',
            sms_alert_recipients: 'BBBBBB',
            sms_min_wallet_alert_template: 'BBBBBB',
            sms_max_wallet_alert_template: 'BBBBBB',
            spare1: 'BBBBBB',
            spare2: 'BBBBBB',
            spare3: 'BBBBBB',
            spare4: 'BBBBBB',
            spare5: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Wallet', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
