import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MicrocreditTestModule } from '../../../../test.module';
import { RepaymentTransferDetailComponent } from 'app/entities/repayment/repayment-transfer/repayment-transfer-detail.component';
import { RepaymentTransfer } from 'app/shared/model/repayment/repayment-transfer.model';

describe('Component Tests', () => {
  describe('RepaymentTransfer Management Detail Component', () => {
    let comp: RepaymentTransferDetailComponent;
    let fixture: ComponentFixture<RepaymentTransferDetailComponent>;
    const route = ({ data: of({ repaymentTransfer: new RepaymentTransfer(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MicrocreditTestModule],
        declarations: [RepaymentTransferDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(RepaymentTransferDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RepaymentTransferDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load repaymentTransfer on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.repaymentTransfer).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
