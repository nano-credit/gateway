import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Data } from '@angular/router';

import { MicrocreditTestModule } from '../../../../test.module';
import { RepaymentTransferComponent } from 'app/entities/repayment/repayment-transfer/repayment-transfer.component';
import { RepaymentTransferService } from 'app/entities/repayment/repayment-transfer/repayment-transfer.service';
import { RepaymentTransfer } from 'app/shared/model/repayment/repayment-transfer.model';

describe('Component Tests', () => {
  describe('RepaymentTransfer Management Component', () => {
    let comp: RepaymentTransferComponent;
    let fixture: ComponentFixture<RepaymentTransferComponent>;
    let service: RepaymentTransferService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MicrocreditTestModule],
        declarations: [RepaymentTransferComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: {
                subscribe: (fn: (value: Data) => void) =>
                  fn({
                    pagingParams: {
                      predicate: 'id',
                      reverse: false,
                      page: 0
                    }
                  })
              }
            }
          }
        ]
      })
        .overrideTemplate(RepaymentTransferComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RepaymentTransferComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RepaymentTransferService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new RepaymentTransfer(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.repaymentTransfers && comp.repaymentTransfers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new RepaymentTransfer(123)],
            headers
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.repaymentTransfers && comp.repaymentTransfers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
