import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {RepaymentTransferService} from 'app/entities/repayment/repayment-transfer/repayment-transfer.service';
import {IRepaymentTransfer, RepaymentTransfer} from 'app/shared/model/repayment/repayment-transfer.model';
import {PaymentState} from 'app/shared/model/enumerations/payment-state.model';
import {RepaymentTransferType} from "app/shared/model/enumerations/repayment-transfer-type.model";

describe('Service Tests', () => {
  describe('RepaymentTransfer Service', () => {
    let injector: TestBed;
    let service: RepaymentTransferService;
    let httpMock: HttpTestingController;
    let elemDefault: IRepaymentTransfer;
    let expectedResult: IRepaymentTransfer | IRepaymentTransfer[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(RepaymentTransferService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new RepaymentTransfer(
        0,
        'AAAAAAA',
        'AAAAAAA',
        0,
        RepaymentTransferType.PARTIAL,
        PaymentState.PENDING,
        'AAAAAAA',
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a RepaymentTransfer', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new RepaymentTransfer()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a RepaymentTransfer', () => {
        const returnedFromService = Object.assign(
          {
            sourceWallet: 'BBBBBB',
            targetWallet: 'BBBBBB',
            amount: 1,
            type: 'BBBBBB',
            paymentState: 'BBBBBB',
            paymentTxId: 'BBBBBB',
            paymentMessage: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of RepaymentTransfer', () => {
        const returnedFromService = Object.assign(
          {
            sourceWallet: 'BBBBBB',
            targetWallet: 'BBBBBB',
            amount: 1,
            type: 'BBBBBB',
            paymentState: 'BBBBBB',
            paymentTxId: 'BBBBBB',
            paymentMessage: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a RepaymentTransfer', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
