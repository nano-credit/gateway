import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { RepaymentFileService } from 'app/entities/repayment/repayment-file/repayment-file.service';
import { IRepaymentFile, RepaymentFile } from 'app/shared/model/repayment/repayment-file.model';
import { RepaymentState } from 'app/shared/model/enumerations/repayment-state.model';
import { RepaymentStatus } from 'app/shared/model/enumerations/repayment-status.model';

describe('Service Tests', () => {
  describe('RepaymentFile Service', () => {
    let injector: TestBed;
    let service: RepaymentFileService;
    let httpMock: HttpTestingController;
    let elemDefault: IRepaymentFile;
    let expectedResult: IRepaymentFile | IRepaymentFile[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(RepaymentFileService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new RepaymentFile(0, 0, 0, currentDate, 0, 0, 0, 0, 0, 0, 0, RepaymentState.ENCOURS, RepaymentStatus.NA, currentDate);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            initialDueDate: currentDate.format(DATE_TIME_FORMAT),
            dueDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a RepaymentFile', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            initialDueDate: currentDate.format(DATE_TIME_FORMAT),
            dueDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            initialDueDate: currentDate,
            dueDate: currentDate
          },
          returnedFromService
        );

        service.create(new RepaymentFile()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a RepaymentFile', () => {
        const returnedFromService = Object.assign(
          {
            loanFileID: 1,
            initialCapital: 1,
            initialDueDate: currentDate.format(DATE_TIME_FORMAT),
            applicationFees: 1,
            pr1Fees: 1,
            pr2Fees: 1,
            capitalOutstanding: 1,
            feesOutstanding: 1,
            pr1Outstanding: 1,
            pr2Outstanding: 1,
            repaymentState: 'BBBBBB',
            repaymentStatus: 'BBBBBB',
            dueDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            initialDueDate: currentDate,
            dueDate: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of RepaymentFile', () => {
        const returnedFromService = Object.assign(
          {
            loanFileID: 1,
            initialCapital: 1,
            initialDueDate: currentDate.format(DATE_TIME_FORMAT),
            applicationFees: 1,
            pr1Fees: 1,
            pr2Fees: 1,
            capitalOutstanding: 1,
            feesOutstanding: 1,
            pr1Outstanding: 1,
            pr2Outstanding: 1,
            repaymentState: 'BBBBBB',
            repaymentStatus: 'BBBBBB',
            dueDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            initialDueDate: currentDate,
            dueDate: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a RepaymentFile', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
