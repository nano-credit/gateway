import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MicrocreditTestModule } from '../../../../test.module';
import { RepaymentFileUpdateComponent } from 'app/entities/repayment/repayment-file/repayment-file-update.component';
import { RepaymentFileService } from 'app/entities/repayment/repayment-file/repayment-file.service';
import { RepaymentFile } from 'app/shared/model/repayment/repayment-file.model';

describe('Component Tests', () => {
  describe('RepaymentFile Management Update Component', () => {
    let comp: RepaymentFileUpdateComponent;
    let fixture: ComponentFixture<RepaymentFileUpdateComponent>;
    let service: RepaymentFileService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MicrocreditTestModule],
        declarations: [RepaymentFileUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(RepaymentFileUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RepaymentFileUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RepaymentFileService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new RepaymentFile(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new RepaymentFile();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
