import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MicrocreditTestModule } from '../../../../test.module';
import { RepaymentFileDetailComponent } from 'app/entities/repayment/repayment-file/repayment-file-detail.component';
import { RepaymentFile } from 'app/shared/model/repayment/repayment-file.model';

describe('Component Tests', () => {
  describe('RepaymentFile Management Detail Component', () => {
    let comp: RepaymentFileDetailComponent;
    let fixture: ComponentFixture<RepaymentFileDetailComponent>;
    const route = ({ data: of({ repaymentFile: new RepaymentFile(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MicrocreditTestModule],
        declarations: [RepaymentFileDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(RepaymentFileDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RepaymentFileDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load repaymentFile on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.repaymentFile).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
