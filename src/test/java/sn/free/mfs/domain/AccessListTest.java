package sn.free.mfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class AccessListTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AccessList.class);
        AccessList accessList1 = new AccessList();
        accessList1.setId(1L);
        AccessList accessList2 = new AccessList();
        accessList2.setId(accessList1.getId());
        assertThat(accessList1).isEqualTo(accessList2);
        accessList2.setId(2L);
        assertThat(accessList1).isNotEqualTo(accessList2);
        accessList1.setId(null);
        assertThat(accessList1).isNotEqualTo(accessList2);
    }
}
