package sn.free.mfs.web.rest;

import sn.free.mfs.MicrocreditApp;
import sn.free.mfs.domain.Wallet;
import sn.free.mfs.repository.WalletRepository;
import sn.free.mfs.service.WalletService;
import sn.free.mfs.service.dto.WalletDTO;
import sn.free.mfs.service.mapper.WalletMapper;
import sn.free.mfs.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static sn.free.mfs.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link WalletResource} REST controller.
 */
@SpringBootTest(classes = MicrocreditApp.class)
public class WalletResourceIT {/*

    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_PIN = "AAAAAAAAAA";
    private static final String UPDATED_PIN = "BBBBBBBBBB";

    private static final String DEFAULT_WALLET_NAME = "AAAAAAAAAA";
    private static final String UPDATED_WALLET_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_WALLET_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_WALLET_TYPE = "BBBBBBBBBB";

    private static final Float DEFAULT_WALLET_MIN_BALANCE = 1F;
    private static final Float UPDATED_WALLET_MIN_BALANCE = 2F;

    private static final Float DEFAULT_WALLET_MAX_BALANCE = 1F;
    private static final Float UPDATED_WALLET_MAX_BALANCE = 2F;

    private static final String DEFAULT_EMAIL_ALERTS_RECIPIENTS_TO = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_ALERTS_RECIPIENTS_TO = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_ALERTS_RECIPIENTS_CC = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_ALERTS_RECIPIENTS_CC = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_MIN_WALLET_ALERT_TEMPLATE = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_MIN_WALLET_ALERT_TEMPLATE = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_MAX_WALLET_ALERT_TEMPLATE = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_MAX_WALLET_ALERT_TEMPLATE = "BBBBBBBBBB";

    private static final String DEFAULT_SMS_ALERT_RECIPIENTS = "AAAAAAAAAA";
    private static final String UPDATED_SMS_ALERT_RECIPIENTS = "BBBBBBBBBB";

    private static final String DEFAULT_SMS_MIN_WALLET_ALERT_TEMPLATE = "AAAAAAAAAA";
    private static final String UPDATED_SMS_MIN_WALLET_ALERT_TEMPLATE = "BBBBBBBBBB";

    private static final String DEFAULT_SMS_MAX_WALLET_ALERT_TEMPLATE = "AAAAAAAAAA";
    private static final String UPDATED_SMS_MAX_WALLET_ALERT_TEMPLATE = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_4 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_4 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_5 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_5 = "BBBBBBBBBB";

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private WalletMapper walletMapper;

    @Autowired
    private WalletService walletService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restWalletMockMvc;

    private Wallet wallet;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final WalletResource walletResource = new WalletResource(walletService);
        this.restWalletMockMvc = MockMvcBuilders.standaloneSetup(walletResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    *//**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     *//*
    public static Wallet createEntity(EntityManager em) {
        Wallet wallet = new Wallet()
            .msisdn(DEFAULT_MSISDN)
            .pin(DEFAULT_PIN)
            .wallet_name(DEFAULT_WALLET_NAME)
            .wallet_type(DEFAULT_WALLET_TYPE)
            .wallet_min_balance(DEFAULT_WALLET_MIN_BALANCE)
            .wallet_max_balance(DEFAULT_WALLET_MAX_BALANCE)
            .email_alerts_recipients_to(DEFAULT_EMAIL_ALERTS_RECIPIENTS_TO)
            .email_alerts_recipients_cc(DEFAULT_EMAIL_ALERTS_RECIPIENTS_CC)
            .email_min_wallet_alert_template(DEFAULT_EMAIL_MIN_WALLET_ALERT_TEMPLATE)
            .email_max_wallet_alert_template(DEFAULT_EMAIL_MAX_WALLET_ALERT_TEMPLATE)
            .sms_alert_recipients(DEFAULT_SMS_ALERT_RECIPIENTS)
            .sms_min_wallet_alert_template(DEFAULT_SMS_MIN_WALLET_ALERT_TEMPLATE)
            .sms_max_wallet_alert_template(DEFAULT_SMS_MAX_WALLET_ALERT_TEMPLATE)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3)
            .spare4(DEFAULT_SPARE_4)
            .spare5(DEFAULT_SPARE_5);
        return wallet;
    }
    *//**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     *//*
    public static Wallet createUpdatedEntity(EntityManager em) {
        Wallet wallet = new Wallet()
            .msisdn(UPDATED_MSISDN)
            .pin(UPDATED_PIN)
            .wallet_name(UPDATED_WALLET_NAME)
            .wallet_type(UPDATED_WALLET_TYPE)
            .wallet_min_balance(UPDATED_WALLET_MIN_BALANCE)
            .wallet_max_balance(UPDATED_WALLET_MAX_BALANCE)
            .email_alerts_recipients_to(UPDATED_EMAIL_ALERTS_RECIPIENTS_TO)
            .email_alerts_recipients_cc(UPDATED_EMAIL_ALERTS_RECIPIENTS_CC)
            .email_min_wallet_alert_template(UPDATED_EMAIL_MIN_WALLET_ALERT_TEMPLATE)
            .email_max_wallet_alert_template(UPDATED_EMAIL_MAX_WALLET_ALERT_TEMPLATE)
            .sms_alert_recipients(UPDATED_SMS_ALERT_RECIPIENTS)
            .sms_min_wallet_alert_template(UPDATED_SMS_MIN_WALLET_ALERT_TEMPLATE)
            .sms_max_wallet_alert_template(UPDATED_SMS_MAX_WALLET_ALERT_TEMPLATE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3)
            .spare4(UPDATED_SPARE_4)
            .spare5(UPDATED_SPARE_5);
        return wallet;
    }

    @BeforeEach
    public void initTest() {
        wallet = createEntity(em);
    }

    @Test
    @Transactional
    public void createWallet() throws Exception {
        int databaseSizeBeforeCreate = walletRepository.findAll().size();

        // Create the Wallet
        WalletDTO walletDTO = walletMapper.toDto(wallet);
        restWalletMockMvc.perform(post("/api/wallets")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(walletDTO)))
            .andExpect(status().isCreated());

        // Validate the Wallet in the database
        List<Wallet> walletList = walletRepository.findAll();
        assertThat(walletList).hasSize(databaseSizeBeforeCreate + 1);
        Wallet testWallet = walletList.get(walletList.size() - 1);
        assertThat(testWallet.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
        assertThat(testWallet.getPin()).isEqualTo(DEFAULT_PIN);
        assertThat(testWallet.getWallet_name()).isEqualTo(DEFAULT_WALLET_NAME);
        assertThat(testWallet.getWallet_type()).isEqualTo(DEFAULT_WALLET_TYPE);
        assertThat(testWallet.getWallet_min_balance()).isEqualTo(DEFAULT_WALLET_MIN_BALANCE);
        assertThat(testWallet.getWallet_max_balance()).isEqualTo(DEFAULT_WALLET_MAX_BALANCE);
        assertThat(testWallet.getEmail_alerts_recipients_to()).isEqualTo(DEFAULT_EMAIL_ALERTS_RECIPIENTS_TO);
        assertThat(testWallet.getEmail_alerts_recipients_cc()).isEqualTo(DEFAULT_EMAIL_ALERTS_RECIPIENTS_CC);
        assertThat(testWallet.getEmail_min_wallet_alert_template()).isEqualTo(DEFAULT_EMAIL_MIN_WALLET_ALERT_TEMPLATE);
        assertThat(testWallet.getEmail_max_wallet_alert_template()).isEqualTo(DEFAULT_EMAIL_MAX_WALLET_ALERT_TEMPLATE);
        assertThat(testWallet.getSms_alert_recipients()).isEqualTo(DEFAULT_SMS_ALERT_RECIPIENTS);
        assertThat(testWallet.getSms_min_wallet_alert_template()).isEqualTo(DEFAULT_SMS_MIN_WALLET_ALERT_TEMPLATE);
        assertThat(testWallet.getSms_max_wallet_alert_template()).isEqualTo(DEFAULT_SMS_MAX_WALLET_ALERT_TEMPLATE);
        assertThat(testWallet.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testWallet.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testWallet.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
        assertThat(testWallet.getSpare4()).isEqualTo(DEFAULT_SPARE_4);
        assertThat(testWallet.getSpare5()).isEqualTo(DEFAULT_SPARE_5);
    }

    @Test
    @Transactional
    public void createWalletWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = walletRepository.findAll().size();

        // Create the Wallet with an existing ID
        wallet.setId(1L);
        WalletDTO walletDTO = walletMapper.toDto(wallet);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWalletMockMvc.perform(post("/api/wallets")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(walletDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Wallet in the database
        List<Wallet> walletList = walletRepository.findAll();
        assertThat(walletList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkMsisdnIsRequired() throws Exception {
        int databaseSizeBeforeTest = walletRepository.findAll().size();
        // set the field null
        wallet.setMsisdn(null);

        // Create the Wallet, which fails.
        WalletDTO walletDTO = walletMapper.toDto(wallet);

        restWalletMockMvc.perform(post("/api/wallets")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(walletDTO)))
            .andExpect(status().isBadRequest());

        List<Wallet> walletList = walletRepository.findAll();
        assertThat(walletList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPinIsRequired() throws Exception {
        int databaseSizeBeforeTest = walletRepository.findAll().size();
        // set the field null
        wallet.setPin(null);

        // Create the Wallet, which fails.
        WalletDTO walletDTO = walletMapper.toDto(wallet);

        restWalletMockMvc.perform(post("/api/wallets")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(walletDTO)))
            .andExpect(status().isBadRequest());

        List<Wallet> walletList = walletRepository.findAll();
        assertThat(walletList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllWallets() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get all the walletList
        restWalletMockMvc.perform(get("/api/wallets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(wallet.getId().intValue())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN)))
            .andExpect(jsonPath("$.[*].pin").value(hasItem(DEFAULT_PIN)))
            .andExpect(jsonPath("$.[*].wallet_name").value(hasItem(DEFAULT_WALLET_NAME)))
            .andExpect(jsonPath("$.[*].wallet_type").value(hasItem(DEFAULT_WALLET_TYPE)))
            .andExpect(jsonPath("$.[*].wallet_min_balance").value(hasItem(DEFAULT_WALLET_MIN_BALANCE.doubleValue())))
            .andExpect(jsonPath("$.[*].wallet_max_balance").value(hasItem(DEFAULT_WALLET_MAX_BALANCE.doubleValue())))
            .andExpect(jsonPath("$.[*].email_alerts_recipients_to").value(hasItem(DEFAULT_EMAIL_ALERTS_RECIPIENTS_TO)))
            .andExpect(jsonPath("$.[*].email_alerts_recipients_cc").value(hasItem(DEFAULT_EMAIL_ALERTS_RECIPIENTS_CC)))
            .andExpect(jsonPath("$.[*].email_min_wallet_alert_template").value(hasItem(DEFAULT_EMAIL_MIN_WALLET_ALERT_TEMPLATE)))
            .andExpect(jsonPath("$.[*].email_max_wallet_alert_template").value(hasItem(DEFAULT_EMAIL_MAX_WALLET_ALERT_TEMPLATE)))
            .andExpect(jsonPath("$.[*].sms_alert_recipients").value(hasItem(DEFAULT_SMS_ALERT_RECIPIENTS)))
            .andExpect(jsonPath("$.[*].sms_min_wallet_alert_template").value(hasItem(DEFAULT_SMS_MIN_WALLET_ALERT_TEMPLATE)))
            .andExpect(jsonPath("$.[*].sms_max_wallet_alert_template").value(hasItem(DEFAULT_SMS_MAX_WALLET_ALERT_TEMPLATE)))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)))
            .andExpect(jsonPath("$.[*].spare4").value(hasItem(DEFAULT_SPARE_4)))
            .andExpect(jsonPath("$.[*].spare5").value(hasItem(DEFAULT_SPARE_5)));
    }

    @Test
    @Transactional
    public void getWallet() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get the wallet
        restWalletMockMvc.perform(get("/api/wallets/{id}", wallet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(wallet.getId().intValue()))
            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN))
            .andExpect(jsonPath("$.pin").value(DEFAULT_PIN))
            .andExpect(jsonPath("$.wallet_name").value(DEFAULT_WALLET_NAME))
            .andExpect(jsonPath("$.wallet_type").value(DEFAULT_WALLET_TYPE))
            .andExpect(jsonPath("$.wallet_min_balance").value(DEFAULT_WALLET_MIN_BALANCE.doubleValue()))
            .andExpect(jsonPath("$.wallet_max_balance").value(DEFAULT_WALLET_MAX_BALANCE.doubleValue()))
            .andExpect(jsonPath("$.email_alerts_recipients_to").value(DEFAULT_EMAIL_ALERTS_RECIPIENTS_TO))
            .andExpect(jsonPath("$.email_alerts_recipients_cc").value(DEFAULT_EMAIL_ALERTS_RECIPIENTS_CC))
            .andExpect(jsonPath("$.email_min_wallet_alert_template").value(DEFAULT_EMAIL_MIN_WALLET_ALERT_TEMPLATE))
            .andExpect(jsonPath("$.email_max_wallet_alert_template").value(DEFAULT_EMAIL_MAX_WALLET_ALERT_TEMPLATE))
            .andExpect(jsonPath("$.sms_alert_recipients").value(DEFAULT_SMS_ALERT_RECIPIENTS))
            .andExpect(jsonPath("$.sms_min_wallet_alert_template").value(DEFAULT_SMS_MIN_WALLET_ALERT_TEMPLATE))
            .andExpect(jsonPath("$.sms_max_wallet_alert_template").value(DEFAULT_SMS_MAX_WALLET_ALERT_TEMPLATE))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3))
            .andExpect(jsonPath("$.spare4").value(DEFAULT_SPARE_4))
            .andExpect(jsonPath("$.spare5").value(DEFAULT_SPARE_5));
    }

    @Test
    @Transactional
    public void getNonExistingWallet() throws Exception {
        // Get the wallet
        restWalletMockMvc.perform(get("/api/wallets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWallet() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        int databaseSizeBeforeUpdate = walletRepository.findAll().size();

        // Update the wallet
        Wallet updatedWallet = walletRepository.findById(wallet.getId()).get();
        // Disconnect from session so that the updates on updatedWallet are not directly saved in db
        em.detach(updatedWallet);
        updatedWallet
            .msisdn(UPDATED_MSISDN)
            .pin(UPDATED_PIN)
            .wallet_name(UPDATED_WALLET_NAME)
            .wallet_type(UPDATED_WALLET_TYPE)
            .wallet_min_balance(UPDATED_WALLET_MIN_BALANCE)
            .wallet_max_balance(UPDATED_WALLET_MAX_BALANCE)
            .email_alerts_recipients_to(UPDATED_EMAIL_ALERTS_RECIPIENTS_TO)
            .email_alerts_recipients_cc(UPDATED_EMAIL_ALERTS_RECIPIENTS_CC)
            .email_min_wallet_alert_template(UPDATED_EMAIL_MIN_WALLET_ALERT_TEMPLATE)
            .email_max_wallet_alert_template(UPDATED_EMAIL_MAX_WALLET_ALERT_TEMPLATE)
            .sms_alert_recipients(UPDATED_SMS_ALERT_RECIPIENTS)
            .sms_min_wallet_alert_template(UPDATED_SMS_MIN_WALLET_ALERT_TEMPLATE)
            .sms_max_wallet_alert_template(UPDATED_SMS_MAX_WALLET_ALERT_TEMPLATE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3)
            .spare4(UPDATED_SPARE_4)
            .spare5(UPDATED_SPARE_5);
        WalletDTO walletDTO = walletMapper.toDto(updatedWallet);

        restWalletMockMvc.perform(put("/api/wallets")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(walletDTO)))
            .andExpect(status().isOk());

        // Validate the Wallet in the database
        List<Wallet> walletList = walletRepository.findAll();
        assertThat(walletList).hasSize(databaseSizeBeforeUpdate);
        Wallet testWallet = walletList.get(walletList.size() - 1);
        assertThat(testWallet.getMsisdn()).isEqualTo(UPDATED_MSISDN);
        assertThat(testWallet.getPin()).isEqualTo(UPDATED_PIN);
        assertThat(testWallet.getWallet_name()).isEqualTo(UPDATED_WALLET_NAME);
        assertThat(testWallet.getWallet_type()).isEqualTo(UPDATED_WALLET_TYPE);
        assertThat(testWallet.getWallet_min_balance()).isEqualTo(UPDATED_WALLET_MIN_BALANCE);
        assertThat(testWallet.getWallet_max_balance()).isEqualTo(UPDATED_WALLET_MAX_BALANCE);
        assertThat(testWallet.getEmail_alerts_recipients_to()).isEqualTo(UPDATED_EMAIL_ALERTS_RECIPIENTS_TO);
        assertThat(testWallet.getEmail_alerts_recipients_cc()).isEqualTo(UPDATED_EMAIL_ALERTS_RECIPIENTS_CC);
        assertThat(testWallet.getEmail_min_wallet_alert_template()).isEqualTo(UPDATED_EMAIL_MIN_WALLET_ALERT_TEMPLATE);
        assertThat(testWallet.getEmail_max_wallet_alert_template()).isEqualTo(UPDATED_EMAIL_MAX_WALLET_ALERT_TEMPLATE);
        assertThat(testWallet.getSms_alert_recipients()).isEqualTo(UPDATED_SMS_ALERT_RECIPIENTS);
        assertThat(testWallet.getSms_min_wallet_alert_template()).isEqualTo(UPDATED_SMS_MIN_WALLET_ALERT_TEMPLATE);
        assertThat(testWallet.getSms_max_wallet_alert_template()).isEqualTo(UPDATED_SMS_MAX_WALLET_ALERT_TEMPLATE);
        assertThat(testWallet.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testWallet.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testWallet.getSpare3()).isEqualTo(UPDATED_SPARE_3);
        assertThat(testWallet.getSpare4()).isEqualTo(UPDATED_SPARE_4);
        assertThat(testWallet.getSpare5()).isEqualTo(UPDATED_SPARE_5);
    }

    @Test
    @Transactional
    public void updateNonExistingWallet() throws Exception {
        int databaseSizeBeforeUpdate = walletRepository.findAll().size();

        // Create the Wallet
        WalletDTO walletDTO = walletMapper.toDto(wallet);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWalletMockMvc.perform(put("/api/wallets")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(walletDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Wallet in the database
        List<Wallet> walletList = walletRepository.findAll();
        assertThat(walletList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteWallet() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        int databaseSizeBeforeDelete = walletRepository.findAll().size();

        // Delete the wallet
        restWalletMockMvc.perform(delete("/api/wallets/{id}", wallet.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Wallet> walletList = walletRepository.findAll();
        assertThat(walletList).hasSize(databaseSizeBeforeDelete - 1);
    }*/
}
