//package sn.free.mfs.web.rest;
//
//import sn.free.mfs.MicrocreditApp;
//import sn.free.mfs.domain.Offer;
//import sn.free.mfs.repository.OfferRepository;
//import sn.free.mfs.service.OfferService;
//import sn.free.mfs.service.dto.OfferDTO;
//import sn.free.mfs.service.mapper.OfferMapper;
//import sn.free.mfs.web.rest.errors.ExceptionTranslator;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.validation.Validator;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static sn.free.mfs.web.rest.TestUtil.createFormattingConversionService;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
///**
// * Integration tests for the {@link OfferResource} REST controller.
// */
//@SpringBootTest(classes = MicrocreditApp.class)
//public class OfferResourceIT {
//
//    private static final String DEFAULT_TENANT_ID = "AAAAAAAAAA";
//    private static final String UPDATED_TENANT_ID = "BBBBBBBBBB";
//
//    private static final String DEFAULT_OFFER_NAME = "AAAAAAAAAA";
//    private static final String UPDATED_OFFER_NAME = "BBBBBBBBBB";
//
//    private static final Boolean DEFAULT_ACTIVATED = false;
//    private static final Boolean UPDATED_ACTIVATED = true;
//
//    private static final Integer DEFAULT_FREE_SENIORITY = 1;
//    private static final Integer UPDATED_FREE_SENIORITY = 2;
//
//    private static final Integer DEFAULT_FREEMONEY_SENIORITY = 1;
//    private static final Integer UPDATED_FREEMONEY_SENIORITY = 2;
//
//    private static final Integer DEFAULT_DEFAULT_LOAN_DURATION = 1;
//    private static final Integer UPDATED_DEFAULT_LOAN_DURATION = 2;
//
//    private static final Integer DEFAULT_MIN_CUST_AGE = 1;
//    private static final Integer UPDATED_MIN_CUST_AGE = 2;
//
//    private static final Integer DEFAULT_MAX_CUST_AGE = 1;
//    private static final Integer UPDATED_MAX_CUST_AGE = 2;
//
//    private static final Integer DEFAULT_MAX_SIMULTANEOUS_LOAN = 1;
//    private static final Integer UPDATED_MAX_SIMULTANEOUS_LOAN = 2;
//
//    private static final Integer DEFAULT_MAX_LOAN_PER_DATE = 1;
//    private static final Integer UPDATED_MAX_LOAN_PER_DATE = 2;
//
//    private static final Integer DEFAULT_MAX_CURRENT_BALANCE = 1;
//    private static final Integer UPDATED_MAX_CURRENT_BALANCE = 2;
//
//    private static final Integer DEFAULT_MIN_AMOUNT = 1;
//    private static final Integer UPDATED_MIN_AMOUNT = 2;
//
//    private static final Integer DEFAULT_MAX_AMOUNT = 1;
//    private static final Integer UPDATED_MAX_AMOUNT = 2;
//
//    private static final Integer DEFAULT_TOTAL_FEES_RATE = 1;
//    private static final Integer UPDATED_TOTAL_FEES_RATE = 2;
//
//    private static final Integer DEFAULT_MIN_REPAYMENT = 1;
//    private static final Integer UPDATED_MIN_REPAYMENT = 2;
//
//    private static final Integer DEFAULT_PR_0_RATE = 1;
//    private static final Integer UPDATED_PR_0_RATE = 2;
//
//    private static final Integer DEFAULT_PR_1_RATE = 1;
//    private static final Integer UPDATED_PR_1_RATE = 2;
//
//    private static final Integer DEFAULT_PR_2_RATE = 1;
//    private static final Integer UPDATED_PR_2_RATE = 2;
//
//    private static final String DEFAULT_MSISDN_ADMIN = "AAAAAAAAAA";
//    private static final String UPDATED_MSISDN_ADMIN = "BBBBBBBBBB";
//
//    private static final String DEFAULT_EMAIL_ADMIN = "AAAAAAAAAA";
//    private static final String UPDATED_EMAIL_ADMIN = "BBBBBBBBBB";
//
//    @Autowired
//    private OfferRepository offerRepository;
//
//    @Autowired
//    private OfferMapper offerMapper;
//
//    @Autowired
//    private OfferService offerService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restOfferMockMvc;
//
//    private Offer offer;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final OfferResource offerResource = new OfferResource(offerService);
//        this.restOfferMockMvc = MockMvcBuilders.standaloneSetup(offerResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static Offer createEntity(EntityManager em) {
//        Offer offer = new Offer()
//            .tenantId(DEFAULT_TENANT_ID)
//            .offerName(DEFAULT_OFFER_NAME)
//            .activated(DEFAULT_ACTIVATED)
//            .freeSeniority(DEFAULT_FREE_SENIORITY)
//            .freemoneySeniority(DEFAULT_FREEMONEY_SENIORITY)
//            .defaultLoanDuration(DEFAULT_DEFAULT_LOAN_DURATION)
//            .minCustAge(DEFAULT_MIN_CUST_AGE)
//            .maxCustAge(DEFAULT_MAX_CUST_AGE)
//            .maxSimultaneousLoan(DEFAULT_MAX_SIMULTANEOUS_LOAN)
//            .maxLoanPerDate(DEFAULT_MAX_LOAN_PER_DATE)
//            .maxCurrentBalance(DEFAULT_MAX_CURRENT_BALANCE)
//            .minAmount(DEFAULT_MIN_AMOUNT)
//            .maxAmount(DEFAULT_MAX_AMOUNT)
//            .totalFeesRate(DEFAULT_TOTAL_FEES_RATE)
//            .minRepayment(DEFAULT_MIN_REPAYMENT)
//            .pr0Rate(DEFAULT_PR_0_RATE)
//            .pr1Rate(DEFAULT_PR_1_RATE)
//            .pr2Rate(DEFAULT_PR_2_RATE)
//            .msisdnAdmin(DEFAULT_MSISDN_ADMIN)
//            .emailAdmin(DEFAULT_EMAIL_ADMIN);
//        return offer;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static Offer createUpdatedEntity(EntityManager em) {
//        Offer offer = new Offer()
//            .tenantId(UPDATED_TENANT_ID)
//            .offerName(UPDATED_OFFER_NAME)
//            .activated(UPDATED_ACTIVATED)
//            .freeSeniority(UPDATED_FREE_SENIORITY)
//            .freemoneySeniority(UPDATED_FREEMONEY_SENIORITY)
//            .defaultLoanDuration(UPDATED_DEFAULT_LOAN_DURATION)
//            .minCustAge(UPDATED_MIN_CUST_AGE)
//            .maxCustAge(UPDATED_MAX_CUST_AGE)
//            .maxSimultaneousLoan(UPDATED_MAX_SIMULTANEOUS_LOAN)
//            .maxLoanPerDate(UPDATED_MAX_LOAN_PER_DATE)
//            .maxCurrentBalance(UPDATED_MAX_CURRENT_BALANCE)
//            .minAmount(UPDATED_MIN_AMOUNT)
//            .maxAmount(UPDATED_MAX_AMOUNT)
//            .totalFeesRate(UPDATED_TOTAL_FEES_RATE)
//            .minRepayment(UPDATED_MIN_REPAYMENT)
//            .pr0Rate(UPDATED_PR_0_RATE)
//            .pr1Rate(UPDATED_PR_1_RATE)
//            .pr2Rate(UPDATED_PR_2_RATE)
//            .msisdnAdmin(UPDATED_MSISDN_ADMIN)
//            .emailAdmin(UPDATED_EMAIL_ADMIN);
//        return offer;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        offer = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createOffer() throws Exception {
//        int databaseSizeBeforeCreate = offerRepository.findAll().size();
//
//        // Create the Offer
//        OfferDTO offerDTO = offerMapper.toDto(offer);
//        restOfferMockMvc.perform(post("/api/offers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(offerDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the Offer in the database
//        List<Offer> offerList = offerRepository.findAll();
//        assertThat(offerList).hasSize(databaseSizeBeforeCreate + 1);
//        Offer testOffer = offerList.get(offerList.size() - 1);
//        assertThat(testOffer.getTenantId()).isEqualTo(DEFAULT_TENANT_ID);
//        assertThat(testOffer.getOfferName()).isEqualTo(DEFAULT_OFFER_NAME);
//        assertThat(testOffer.isActivated()).isEqualTo(DEFAULT_ACTIVATED);
//        assertThat(testOffer.getFreeSeniority()).isEqualTo(DEFAULT_FREE_SENIORITY);
//        assertThat(testOffer.getFreemoneySeniority()).isEqualTo(DEFAULT_FREEMONEY_SENIORITY);
//        assertThat(testOffer.getDefaultLoanDuration()).isEqualTo(DEFAULT_DEFAULT_LOAN_DURATION);
//        assertThat(testOffer.getMinCustAge()).isEqualTo(DEFAULT_MIN_CUST_AGE);
//        assertThat(testOffer.getMaxCustAge()).isEqualTo(DEFAULT_MAX_CUST_AGE);
//        assertThat(testOffer.getMaxSimultaneousLoan()).isEqualTo(DEFAULT_MAX_SIMULTANEOUS_LOAN);
//        assertThat(testOffer.getMaxLoanPerDate()).isEqualTo(DEFAULT_MAX_LOAN_PER_DATE);
//        assertThat(testOffer.getMaxCurrentBalance()).isEqualTo(DEFAULT_MAX_CURRENT_BALANCE);
//        assertThat(testOffer.getMinAmount()).isEqualTo(DEFAULT_MIN_AMOUNT);
//        assertThat(testOffer.getMaxAmount()).isEqualTo(DEFAULT_MAX_AMOUNT);
//        assertThat(testOffer.getTotalFeesRate()).isEqualTo(DEFAULT_TOTAL_FEES_RATE);
//        assertThat(testOffer.getMinRepayment()).isEqualTo(DEFAULT_MIN_REPAYMENT);
//        assertThat(testOffer.getPr0Rate()).isEqualTo(DEFAULT_PR_0_RATE);
//        assertThat(testOffer.getPr1Rate()).isEqualTo(DEFAULT_PR_1_RATE);
//        assertThat(testOffer.getPr2Rate()).isEqualTo(DEFAULT_PR_2_RATE);
//        assertThat(testOffer.getMsisdnAdmin()).isEqualTo(DEFAULT_MSISDN_ADMIN);
//        assertThat(testOffer.getEmailAdmin()).isEqualTo(DEFAULT_EMAIL_ADMIN);
//    }
//
//    @Test
//    @Transactional
//    public void createOfferWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = offerRepository.findAll().size();
//
//        // Create the Offer with an existing ID
//        offer.setId(1L);
//        OfferDTO offerDTO = offerMapper.toDto(offer);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restOfferMockMvc.perform(post("/api/offers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(offerDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the Offer in the database
//        List<Offer> offerList = offerRepository.findAll();
//        assertThat(offerList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void checkTenantIdIsRequired() throws Exception {
//        int databaseSizeBeforeTest = offerRepository.findAll().size();
//        // set the field null
//        offer.setTenantId(null);
//
//        // Create the Offer, which fails.
//        OfferDTO offerDTO = offerMapper.toDto(offer);
//
//        restOfferMockMvc.perform(post("/api/offers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(offerDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<Offer> offerList = offerRepository.findAll();
//        assertThat(offerList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkOfferNameIsRequired() throws Exception {
//        int databaseSizeBeforeTest = offerRepository.findAll().size();
//        // set the field null
//        offer.setOfferName(null);
//
//        // Create the Offer, which fails.
//        OfferDTO offerDTO = offerMapper.toDto(offer);
//
//        restOfferMockMvc.perform(post("/api/offers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(offerDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<Offer> offerList = offerRepository.findAll();
//        assertThat(offerList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkTotalFeesRateIsRequired() throws Exception {
//        int databaseSizeBeforeTest = offerRepository.findAll().size();
//        // set the field null
//        offer.setTotalFeesRate(null);
//
//        // Create the Offer, which fails.
//        OfferDTO offerDTO = offerMapper.toDto(offer);
//
//        restOfferMockMvc.perform(post("/api/offers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(offerDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<Offer> offerList = offerRepository.findAll();
//        assertThat(offerList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkMinRepaymentIsRequired() throws Exception {
//        int databaseSizeBeforeTest = offerRepository.findAll().size();
//        // set the field null
//        offer.setMinRepayment(null);
//
//        // Create the Offer, which fails.
//        OfferDTO offerDTO = offerMapper.toDto(offer);
//
//        restOfferMockMvc.perform(post("/api/offers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(offerDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<Offer> offerList = offerRepository.findAll();
//        assertThat(offerList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkPr0RateIsRequired() throws Exception {
//        int databaseSizeBeforeTest = offerRepository.findAll().size();
//        // set the field null
//        offer.setPr0Rate(null);
//
//        // Create the Offer, which fails.
//        OfferDTO offerDTO = offerMapper.toDto(offer);
//
//        restOfferMockMvc.perform(post("/api/offers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(offerDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<Offer> offerList = offerRepository.findAll();
//        assertThat(offerList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkPr1RateIsRequired() throws Exception {
//        int databaseSizeBeforeTest = offerRepository.findAll().size();
//        // set the field null
//        offer.setPr1Rate(null);
//
//        // Create the Offer, which fails.
//        OfferDTO offerDTO = offerMapper.toDto(offer);
//
//        restOfferMockMvc.perform(post("/api/offers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(offerDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<Offer> offerList = offerRepository.findAll();
//        assertThat(offerList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkPr2RateIsRequired() throws Exception {
//        int databaseSizeBeforeTest = offerRepository.findAll().size();
//        // set the field null
//        offer.setPr2Rate(null);
//
//        // Create the Offer, which fails.
//        OfferDTO offerDTO = offerMapper.toDto(offer);
//
//        restOfferMockMvc.perform(post("/api/offers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(offerDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<Offer> offerList = offerRepository.findAll();
//        assertThat(offerList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkEmailAdminIsRequired() throws Exception {
//        int databaseSizeBeforeTest = offerRepository.findAll().size();
//        // set the field null
//        offer.setEmailAdmin(null);
//
//        // Create the Offer, which fails.
//        OfferDTO offerDTO = offerMapper.toDto(offer);
//
//        restOfferMockMvc.perform(post("/api/offers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(offerDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<Offer> offerList = offerRepository.findAll();
//        assertThat(offerList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void getAllOffers() throws Exception {
//        // Initialize the database
//        offerRepository.saveAndFlush(offer);
//
//        // Get all the offerList
//        restOfferMockMvc.perform(get("/api/offers?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(offer.getId().intValue())))
//            .andExpect(jsonPath("$.[*].tenantId").value(hasItem(DEFAULT_TENANT_ID)))
//            .andExpect(jsonPath("$.[*].offerName").value(hasItem(DEFAULT_OFFER_NAME)))
//            .andExpect(jsonPath("$.[*].activated").value(hasItem(DEFAULT_ACTIVATED.booleanValue())))
//            .andExpect(jsonPath("$.[*].freeSeniority").value(hasItem(DEFAULT_FREE_SENIORITY)))
//            .andExpect(jsonPath("$.[*].freemoneySeniority").value(hasItem(DEFAULT_FREEMONEY_SENIORITY)))
//            .andExpect(jsonPath("$.[*].defaultLoanDuration").value(hasItem(DEFAULT_DEFAULT_LOAN_DURATION)))
//            .andExpect(jsonPath("$.[*].minCustAge").value(hasItem(DEFAULT_MIN_CUST_AGE)))
//            .andExpect(jsonPath("$.[*].maxCustAge").value(hasItem(DEFAULT_MAX_CUST_AGE)))
//            .andExpect(jsonPath("$.[*].maxSimultaneousLoan").value(hasItem(DEFAULT_MAX_SIMULTANEOUS_LOAN)))
//            .andExpect(jsonPath("$.[*].maxLoanPerDate").value(hasItem(DEFAULT_MAX_LOAN_PER_DATE)))
//            .andExpect(jsonPath("$.[*].maxCurrentBalance").value(hasItem(DEFAULT_MAX_CURRENT_BALANCE)))
//            .andExpect(jsonPath("$.[*].minAmount").value(hasItem(DEFAULT_MIN_AMOUNT)))
//            .andExpect(jsonPath("$.[*].maxAmount").value(hasItem(DEFAULT_MAX_AMOUNT)))
//            .andExpect(jsonPath("$.[*].totalFeesRate").value(hasItem(DEFAULT_TOTAL_FEES_RATE)))
//            .andExpect(jsonPath("$.[*].minRepayment").value(hasItem(DEFAULT_MIN_REPAYMENT)))
//            .andExpect(jsonPath("$.[*].pr0Rate").value(hasItem(DEFAULT_PR_0_RATE)))
//            .andExpect(jsonPath("$.[*].pr1Rate").value(hasItem(DEFAULT_PR_1_RATE)))
//            .andExpect(jsonPath("$.[*].pr2Rate").value(hasItem(DEFAULT_PR_2_RATE)))
//            .andExpect(jsonPath("$.[*].msisdnAdmin").value(hasItem(DEFAULT_MSISDN_ADMIN)))
//            .andExpect(jsonPath("$.[*].emailAdmin").value(hasItem(DEFAULT_EMAIL_ADMIN)));
//    }
//
//    @Test
//    @Transactional
//    public void getOffer() throws Exception {
//        // Initialize the database
//        offerRepository.saveAndFlush(offer);
//
//        // Get the offer
//        restOfferMockMvc.perform(get("/api/offers/{id}", offer.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
//            .andExpect(jsonPath("$.id").value(offer.getId().intValue()))
//            .andExpect(jsonPath("$.tenantId").value(DEFAULT_TENANT_ID))
//            .andExpect(jsonPath("$.offerName").value(DEFAULT_OFFER_NAME))
//            .andExpect(jsonPath("$.activated").value(DEFAULT_ACTIVATED.booleanValue()))
//            .andExpect(jsonPath("$.freeSeniority").value(DEFAULT_FREE_SENIORITY))
//            .andExpect(jsonPath("$.freemoneySeniority").value(DEFAULT_FREEMONEY_SENIORITY))
//            .andExpect(jsonPath("$.defaultLoanDuration").value(DEFAULT_DEFAULT_LOAN_DURATION))
//            .andExpect(jsonPath("$.minCustAge").value(DEFAULT_MIN_CUST_AGE))
//            .andExpect(jsonPath("$.maxCustAge").value(DEFAULT_MAX_CUST_AGE))
//            .andExpect(jsonPath("$.maxSimultaneousLoan").value(DEFAULT_MAX_SIMULTANEOUS_LOAN))
//            .andExpect(jsonPath("$.maxLoanPerDate").value(DEFAULT_MAX_LOAN_PER_DATE))
//            .andExpect(jsonPath("$.maxCurrentBalance").value(DEFAULT_MAX_CURRENT_BALANCE))
//            .andExpect(jsonPath("$.minAmount").value(DEFAULT_MIN_AMOUNT))
//            .andExpect(jsonPath("$.maxAmount").value(DEFAULT_MAX_AMOUNT))
//            .andExpect(jsonPath("$.totalFeesRate").value(DEFAULT_TOTAL_FEES_RATE))
//            .andExpect(jsonPath("$.minRepayment").value(DEFAULT_MIN_REPAYMENT))
//            .andExpect(jsonPath("$.pr0Rate").value(DEFAULT_PR_0_RATE))
//            .andExpect(jsonPath("$.pr1Rate").value(DEFAULT_PR_1_RATE))
//            .andExpect(jsonPath("$.pr2Rate").value(DEFAULT_PR_2_RATE))
//            .andExpect(jsonPath("$.msisdnAdmin").value(DEFAULT_MSISDN_ADMIN))
//            .andExpect(jsonPath("$.emailAdmin").value(DEFAULT_EMAIL_ADMIN));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingOffer() throws Exception {
//        // Get the offer
//        restOfferMockMvc.perform(get("/api/offers/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateOffer() throws Exception {
//        // Initialize the database
//        offerRepository.saveAndFlush(offer);
//
//        int databaseSizeBeforeUpdate = offerRepository.findAll().size();
//
//        // Update the offer
//        Offer updatedOffer = offerRepository.findById(offer.getId()).get();
//        // Disconnect from session so that the updates on updatedOffer are not directly saved in db
//        em.detach(updatedOffer);
//        updatedOffer
//            .tenantId(UPDATED_TENANT_ID)
//            .offerName(UPDATED_OFFER_NAME)
//            .activated(UPDATED_ACTIVATED)
//            .freeSeniority(UPDATED_FREE_SENIORITY)
//            .freemoneySeniority(UPDATED_FREEMONEY_SENIORITY)
//            .defaultLoanDuration(UPDATED_DEFAULT_LOAN_DURATION)
//            .minCustAge(UPDATED_MIN_CUST_AGE)
//            .maxCustAge(UPDATED_MAX_CUST_AGE)
//            .maxSimultaneousLoan(UPDATED_MAX_SIMULTANEOUS_LOAN)
//            .maxLoanPerDate(UPDATED_MAX_LOAN_PER_DATE)
//            .maxCurrentBalance(UPDATED_MAX_CURRENT_BALANCE)
//            .minAmount(UPDATED_MIN_AMOUNT)
//            .maxAmount(UPDATED_MAX_AMOUNT)
//            .totalFeesRate(UPDATED_TOTAL_FEES_RATE)
//            .minRepayment(UPDATED_MIN_REPAYMENT)
//            .pr0Rate(UPDATED_PR_0_RATE)
//            .pr1Rate(UPDATED_PR_1_RATE)
//            .pr2Rate(UPDATED_PR_2_RATE)
//            .msisdnAdmin(UPDATED_MSISDN_ADMIN)
//            .emailAdmin(UPDATED_EMAIL_ADMIN);
//        OfferDTO offerDTO = offerMapper.toDto(updatedOffer);
//
//        restOfferMockMvc.perform(put("/api/offers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(offerDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the Offer in the database
//        List<Offer> offerList = offerRepository.findAll();
//        assertThat(offerList).hasSize(databaseSizeBeforeUpdate);
//        Offer testOffer = offerList.get(offerList.size() - 1);
//        assertThat(testOffer.getTenantId()).isEqualTo(UPDATED_TENANT_ID);
//        assertThat(testOffer.getOfferName()).isEqualTo(UPDATED_OFFER_NAME);
//        assertThat(testOffer.isActivated()).isEqualTo(UPDATED_ACTIVATED);
//        assertThat(testOffer.getFreeSeniority()).isEqualTo(UPDATED_FREE_SENIORITY);
//        assertThat(testOffer.getFreemoneySeniority()).isEqualTo(UPDATED_FREEMONEY_SENIORITY);
//        assertThat(testOffer.getDefaultLoanDuration()).isEqualTo(UPDATED_DEFAULT_LOAN_DURATION);
//        assertThat(testOffer.getMinCustAge()).isEqualTo(UPDATED_MIN_CUST_AGE);
//        assertThat(testOffer.getMaxCustAge()).isEqualTo(UPDATED_MAX_CUST_AGE);
//        assertThat(testOffer.getMaxSimultaneousLoan()).isEqualTo(UPDATED_MAX_SIMULTANEOUS_LOAN);
//        assertThat(testOffer.getMaxLoanPerDate()).isEqualTo(UPDATED_MAX_LOAN_PER_DATE);
//        assertThat(testOffer.getMaxCurrentBalance()).isEqualTo(UPDATED_MAX_CURRENT_BALANCE);
//        assertThat(testOffer.getMinAmount()).isEqualTo(UPDATED_MIN_AMOUNT);
//        assertThat(testOffer.getMaxAmount()).isEqualTo(UPDATED_MAX_AMOUNT);
//        assertThat(testOffer.getTotalFeesRate()).isEqualTo(UPDATED_TOTAL_FEES_RATE);
//        assertThat(testOffer.getMinRepayment()).isEqualTo(UPDATED_MIN_REPAYMENT);
//        assertThat(testOffer.getPr0Rate()).isEqualTo(UPDATED_PR_0_RATE);
//        assertThat(testOffer.getPr1Rate()).isEqualTo(UPDATED_PR_1_RATE);
//        assertThat(testOffer.getPr2Rate()).isEqualTo(UPDATED_PR_2_RATE);
//        assertThat(testOffer.getMsisdnAdmin()).isEqualTo(UPDATED_MSISDN_ADMIN);
//        assertThat(testOffer.getEmailAdmin()).isEqualTo(UPDATED_EMAIL_ADMIN);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingOffer() throws Exception {
//        int databaseSizeBeforeUpdate = offerRepository.findAll().size();
//
//        // Create the Offer
//        OfferDTO offerDTO = offerMapper.toDto(offer);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restOfferMockMvc.perform(put("/api/offers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(offerDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the Offer in the database
//        List<Offer> offerList = offerRepository.findAll();
//        assertThat(offerList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteOffer() throws Exception {
//        // Initialize the database
//        offerRepository.saveAndFlush(offer);
//
//        int databaseSizeBeforeDelete = offerRepository.findAll().size();
//
//        // Delete the offer
//        restOfferMockMvc.perform(delete("/api/offers/{id}", offer.getId())
//            .accept(TestUtil.APPLICATION_JSON))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<Offer> offerList = offerRepository.findAll();
//        assertThat(offerList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//}
