package sn.free.mfs.web.rest;

import sn.free.mfs.MicrocreditApp;
import sn.free.mfs.domain.AccessList;
import sn.free.mfs.repository.AccessListRepository;
import sn.free.mfs.service.AccessListService;
import sn.free.mfs.service.dto.AccessListDTO;
import sn.free.mfs.service.mapper.AccessListMapper;
import sn.free.mfs.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static sn.free.mfs.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import sn.free.mfs.domain.enumeration.AccessStatus;
/**
 * Integration tests for the {@link AccessListResource} REST controller.
 */
@SpringBootTest(classes = MicrocreditApp.class)
public class AccessListResourceIT {

    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_CIN = "AAAAAAAAAA";
    private static final String UPDATED_CIN = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final AccessStatus DEFAULT_STATUS = AccessStatus.BLACKLISTED;
    private static final AccessStatus UPDATED_STATUS = AccessStatus.WHITELISTED;

    @Autowired
    private AccessListRepository accessListRepository;

    @Autowired
    private AccessListMapper accessListMapper;

    @Autowired
    private AccessListService accessListService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAccessListMockMvc;

    private AccessList accessList;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AccessListResource accessListResource = new AccessListResource(accessListService,accessListRepository);
        this.restAccessListMockMvc = MockMvcBuilders.standaloneSetup(accessListResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AccessList createEntity(EntityManager em) {
        AccessList accessList = new AccessList()
            .msisdn(DEFAULT_MSISDN)
            .cin(DEFAULT_CIN)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .email(DEFAULT_EMAIL)
            .status(DEFAULT_STATUS);
        return accessList;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AccessList createUpdatedEntity(EntityManager em) {
        AccessList accessList = new AccessList()
            .msisdn(UPDATED_MSISDN)
            .cin(UPDATED_CIN)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .status(UPDATED_STATUS);
        return accessList;
    }

    @BeforeEach
    public void initTest() {
        accessList = createEntity(em);
    }

    @Test
    @Transactional
    public void createAccessList() throws Exception {
        int databaseSizeBeforeCreate = accessListRepository.findAll().size();

        // Create the AccessList
        AccessListDTO accessListDTO = accessListMapper.toDto(accessList);
        restAccessListMockMvc.perform(post("/api/access-lists")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(accessListDTO)))
            .andExpect(status().isCreated());

        // Validate the AccessList in the database
        List<AccessList> accessListList = accessListRepository.findAll();
        assertThat(accessListList).hasSize(databaseSizeBeforeCreate + 1);
        AccessList testAccessList = accessListList.get(accessListList.size() - 1);
        assertThat(testAccessList.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
        assertThat(testAccessList.getCin()).isEqualTo(DEFAULT_CIN);
        assertThat(testAccessList.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testAccessList.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testAccessList.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testAccessList.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createAccessListWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = accessListRepository.findAll().size();

        // Create the AccessList with an existing ID
        accessList.setId(1L);
        AccessListDTO accessListDTO = accessListMapper.toDto(accessList);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAccessListMockMvc.perform(post("/api/access-lists")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(accessListDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AccessList in the database
        List<AccessList> accessListList = accessListRepository.findAll();
        assertThat(accessListList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAccessLists() throws Exception {
        // Initialize the database
        accessListRepository.saveAndFlush(accessList);

        // Get all the accessListList
        restAccessListMockMvc.perform(get("/api/access-lists?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(accessList.getId().intValue())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN)))
            .andExpect(jsonPath("$.[*].cin").value(hasItem(DEFAULT_CIN)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getAccessList() throws Exception {
        // Initialize the database
        accessListRepository.saveAndFlush(accessList);

        // Get the accessList
        restAccessListMockMvc.perform(get("/api/access-lists/{id}", accessList.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(accessList.getId().intValue()))
            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN))
            .andExpect(jsonPath("$.cin").value(DEFAULT_CIN))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAccessList() throws Exception {
        // Get the accessList
        restAccessListMockMvc.perform(get("/api/access-lists/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAccessList() throws Exception {
        // Initialize the database
        accessListRepository.saveAndFlush(accessList);

        int databaseSizeBeforeUpdate = accessListRepository.findAll().size();

        // Update the accessList
        AccessList updatedAccessList = accessListRepository.findById(accessList.getId()).get();
        // Disconnect from session so that the updates on updatedAccessList are not directly saved in db
        em.detach(updatedAccessList);
        updatedAccessList
            .msisdn(UPDATED_MSISDN)
            .cin(UPDATED_CIN)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .status(UPDATED_STATUS);
        AccessListDTO accessListDTO = accessListMapper.toDto(updatedAccessList);

        restAccessListMockMvc.perform(put("/api/access-lists")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(accessListDTO)))
            .andExpect(status().isOk());

        // Validate the AccessList in the database
        List<AccessList> accessListList = accessListRepository.findAll();
        assertThat(accessListList).hasSize(databaseSizeBeforeUpdate);
        AccessList testAccessList = accessListList.get(accessListList.size() - 1);
        assertThat(testAccessList.getMsisdn()).isEqualTo(UPDATED_MSISDN);
        assertThat(testAccessList.getCin()).isEqualTo(UPDATED_CIN);
        assertThat(testAccessList.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testAccessList.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testAccessList.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testAccessList.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingAccessList() throws Exception {
        int databaseSizeBeforeUpdate = accessListRepository.findAll().size();

        // Create the AccessList
        AccessListDTO accessListDTO = accessListMapper.toDto(accessList);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAccessListMockMvc.perform(put("/api/access-lists")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(accessListDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AccessList in the database
        List<AccessList> accessListList = accessListRepository.findAll();
        assertThat(accessListList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAccessList() throws Exception {
        // Initialize the database
        accessListRepository.saveAndFlush(accessList);

        int databaseSizeBeforeDelete = accessListRepository.findAll().size();

        // Delete the accessList
        restAccessListMockMvc.perform(delete("/api/access-lists/{id}", accessList.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AccessList> accessListList = accessListRepository.findAll();
        assertThat(accessListList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
