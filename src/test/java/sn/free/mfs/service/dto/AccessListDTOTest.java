package sn.free.mfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class AccessListDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AccessListDTO.class);
        AccessListDTO accessListDTO1 = new AccessListDTO();
        accessListDTO1.setId(1L);
        AccessListDTO accessListDTO2 = new AccessListDTO();
        assertThat(accessListDTO1).isNotEqualTo(accessListDTO2);
        accessListDTO2.setId(accessListDTO1.getId());
        assertThat(accessListDTO1).isEqualTo(accessListDTO2);
        accessListDTO2.setId(2L);
        assertThat(accessListDTO1).isNotEqualTo(accessListDTO2);
        accessListDTO1.setId(null);
        assertThat(accessListDTO1).isNotEqualTo(accessListDTO2);
    }
}
