package sn.free.mfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class AccessListMapperTest {

    private AccessListMapper accessListMapper;

    @BeforeEach
    public void setUp() {
        accessListMapper = new AccessListMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(accessListMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(accessListMapper.fromId(null)).isNull();
    }
}
